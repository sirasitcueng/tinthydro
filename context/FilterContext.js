import React, { createContext, useContext, useEffect, useReducer } from "react";
import { StationContext } from "./StationContext";

export const FilterContext = createContext({});

const initialState = {
    searchTxt : '',
    searchDashboard : '',
    isIotFilter : true,
    isWellFilter : true,
    isRainFilter : true,
    isNamtaFilter : true,
};

const reducer = (state, action) => {
    switch (action.type) {
        case "SET_SEARCH_TEXT":
            return {
                ...state, 
                searchTxt : action.payload,
            }
        case "SET_SEARCH_DASHBOARD":
            return {
                ...state, 
                searchDashboard : action.payload,
            }
        case "SET_IOT_FILTER":
            return {
                ...state, 
                isIotFilter : action.payload,
            }
        case "SET_WELL_FILTER":
            return {
                ...state, 
                isWellFilter : action.payload,
            }
        case "SET_RAIN_FILTER":
            return {
                ...state, 
                isRainFilter : action.payload,
            }
        case "SET_NAMTA_FILTER":
            return {
                ...state, 
                isNamtaFilter : action.payload,
            }
        default:
            break;
    }
}


export const FilterProvider = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState);
    const { searchTxt, searchDashboard, isIotFilter, isWellFilter, isRainFilter, isNamtaFilter } = state;
    const { 
        iotMarker, wellMarker, namtaMarker, rainMarker, 
        wellStationData, namtaStationData, setShowWellSample, setShowNamtaSample, 
        iotStation, rainStationData, setShowIotStation, setShowRainStation,
    } = useContext(StationContext);

    const setSearchTxt = payload => dispatch({ type: "SET_SEARCH_TEXT", payload });
    const setSearchDashboard = payload => dispatch({ type: "SET_SEARCH_DASHBOARD", payload });

    const setIotFilter = payload => dispatch({ type: "SET_IOT_FILTER", payload });
    const setWellFilter = payload => dispatch({ type: "SET_WELL_FILTER", payload });
    const setRainFilter = payload => dispatch({ type: "SET_RAIN_FILTER", payload });
    const setNamtaFilter = payload => dispatch({ type: "SET_NAMTA_FILTER", payload });

    useEffect(() => {
        return () => {
            setSearchTxt('');
            setSearchDashboard('');
            setIotFilter(true);
            setWellFilter(true);
            setRainFilter(true);
            setNamtaFilter(true);
        }
    }, []);

    const isSearchFound = (label) => {
        const textSearch = searchTxt.toLowerCase();
        return textSearch === '' ? true : label ? label.toLowerCase().indexOf(textSearch) !== -1 : true;
    }

    const isDashboardSearchFound = (label) => {
        const textSearch = searchDashboard.toLowerCase();
        return textSearch === '' ? true : label ? label.toLowerCase().indexOf(textSearch) !== -1 : true;
    }

    useEffect(() => {
        setShowIotStation(
            Object.values(iotStation).map(({_id, __v,  isOnline, ...station}) => ({...station, isOnline: isOnline ? 'Online' : 'Offline'}))
            .filter(s => isDashboardSearchFound(s.siteName) || isDashboardSearchFound(s.IMEI) )
        );
        setShowRainStation(
            Object.values(rainStationData).map(({_id, __v, keyName, ...station}) => ({...station}))
            .filter(s => isDashboardSearchFound(s.name) || isDashboardSearchFound(s.code) )
        );
        setShowWellSample(
            Object.values(wellStationData).map(({__v, samplingDate, ...station}) => ({...station}))
            .filter(s => isDashboardSearchFound(s.sampleSiteName) || isDashboardSearchFound(s.sampleCode) || isDashboardSearchFound(s.projectNumber))
        );
        setShowNamtaSample(
            Object.values(namtaStationData).map(({__v, samplingDate, ...station}) => ({...station}))
            .filter(s => isDashboardSearchFound(s.sampleSiteName) || isDashboardSearchFound(s.sampleCode) || isDashboardSearchFound(s.projectNumber))
        );
    }, [searchDashboard, iotStation, rainStationData, wellStationData, namtaStationData]);

    useEffect(() => {
        iotMarker.forEach(m => m.setVisible(
            (isSearchFound(m.info.siteName) || isSearchFound(m.info.IMEI)) && isIotFilter
        ));
    }, [isIotFilter, searchTxt]);

    useEffect(() => {
        wellMarker.forEach(m => m.setVisible(
            (isSearchFound(m.info.sampleSiteName) || isSearchFound(m.info.sampleCode) || isSearchFound(m.info.projectNumber)) && isWellFilter
        ));
    }, [isWellFilter, searchTxt]);

    useEffect(() => {
        rainMarker.forEach(m => m.setVisible(
            (isSearchFound(m.info.name) || isSearchFound(m.info.code)) && isRainFilter
        ));
    }, [isRainFilter, searchTxt])

    useEffect(() => {
        namtaMarker.forEach(m => m.setVisible(
            (isSearchFound(m.info.sampleSiteName) || isSearchFound(m.info.sampleCode) || isSearchFound(m.info.projectNumber)) && isNamtaFilter
        ));
    }, [isNamtaFilter, searchTxt])


    return (
        <FilterContext.Provider value={{
            searchTxt, searchDashboard, setSearchTxt, setSearchDashboard,
            isIotFilter, isWellFilter, isRainFilter, isNamtaFilter, setIotFilter, setWellFilter, setRainFilter, setNamtaFilter,
        }}>
            {children}
        </FilterContext.Provider>
    )
}
