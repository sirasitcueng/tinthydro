const physical = {
    waterTable : 'Groundwater level (m)', 
    pH : 'pH',
    ec : 'EC (µS/cm)', 
    temperature : 'Temp.(°C)',
    do : 'DO (mg/L)', 
    orp : 'ORP (mV)', 
    alkalinity : 'Alkalinity (mg/L)',
}

const {waterTable, ...riverPhysical} = physical;

const chemical = {
    ca : 'Ca (mg/L)',
    mg : 'Mg (mg/L)',
    k : 'K (mg/L)',
    na : 'Na (mg/L)',
    fe : 'Fe (mg/L)',
    mn : 'Mn (mg/L)',
    hco3 : 'HCO₃ (mg/L)',
    co3 : 'CO₃ (mg/L)',
    cl : 'Cl (mg/L)',
    no3 : 'NO₃ (mg/L)',
    so4 : 'SO₄ (mg/L)',
    siO2 : 'SiO₂ (mg/L)',
}
const isotope = {
    o18 : '¹⁸O (‰ V-SMOW)',
    h2 : '²H (‰ V-SMOW)',
    h3 : '³H (TU)',
    c13Vpbd : '¹³C (‰ V-PBD)',
    c14Pmc : '¹⁴C (pMC)',
    c14Year : '¹⁴C (year, B.P.)',
}

const groundwaterField = {
    physical,
    chemical,
    isotope,
    all : {
        ...physical, ...chemical, ...isotope
    }
}

const riverWaterField = {
    physical : riverPhysical,
    chemical,
    isotope,
    all : {
        ...riverPhysical, ...chemical, ...isotope
    }
}

export const sampleDataField = {
    well : groundwaterField,
    namta : riverWaterField,
}

export const sampleField = {
    projectNumber : 'Project Number',
    sampleCode : 'Sample Code',
    sampleSiteName : 'Sample Site Name',
    lat : 'Latitude',
    lng : 'Longitude',
    developingDepth : 'Developing Depth (m)',
    screenDepth : 'Screen Depth (m)',
    altitude : 'Altitude (m)',
    aquiferName : 'Aquifer Name', 
}

export const stationKeyPasteArray = [...Object.keys(sampleField)]
export const dataKeyPasteArray = [...Object.keys(sampleField), 'samplingDate', ...Object.keys(groundwaterField.all), ]
export const sampleHeader = ['#', ...Object.values(sampleField)]

export const initialTypingState = Object.keys(groundwaterField.all).reduce((obj, val) => ({...obj, [val] : ''}), { samplingDate : '' })






export const wellDataSchemaObj = Object.keys(groundwaterField.all).reduce((obj, val) => ({...obj, [val] : String}), {
    sampleCode : String,
    sampleSiteName : String,
    samplingDate : String,
})

export const namtaDataSchemaObj = Object.keys(riverWaterField.all).reduce((obj, val) => ({...obj, [val] : String}), {
    sampleCode : String,
    sampleSiteName : String,
    samplingDate : String,
})

export const stationDataSchemaObj = Object.keys(sampleField).reduce((obj, val) => ({...obj, [val] : String}), {})