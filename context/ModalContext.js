import React, { useReducer, createContext, useEffect } from 'react';

export const ModalContext = createContext({});

const initialState = {
    showIotModal : false,
    showWellModal : false,
    showRainModal : false,
    showNamtaModal : false,
}

const reducer = (state, action) => {
    switch (action.type) {
        case "IOT_MODAL":
            return {
                ...state, 
                showIotModal : action.payload,
            }
        case "WELL_MODAL":
            return {
                ...state, 
                showWellModal : action.payload,
            }
        case "RAIN_MODAL":
            return {
                ...state,
                showRainModal : action.payload,
            }
        case "NAMTA_MODAL":
            return {
                ...state,
                showNamtaModal : action.payload,
            }
        case "IOT_DATA":
            return {
                ...state, 
                iotModalData : action.payload,
            }
        case "WELL_DATA":
            return {
                ...state, 
                wellModalData : action.payload,
            }
        case "RAIN_DATA":
            return {
                ...state,
                rainModalData : action.payload,
            }      
        case "NAMTA_DATA":
            return {
                ...state,
                namtaModalData : action.payload,
            }   
        default:
            break;
    }
}

export const ModalProvider = ({ children }) => {

    const [state, dispatch] = useReducer(reducer, initialState);
    const { showIotModal, showWellModal, showRainModal, showNamtaModal,
         iotModalData, wellModalData, rainModalData, namtaModalData } = state;

    const setShowIotModal = payload => dispatch({ type: "IOT_MODAL", payload });
    const setShowWellModal = payload => dispatch({ type: "WELL_MODAL", payload });
    const setShowRainModal = payload => dispatch({ type: "RAIN_MODAL", payload });
    const setShowNamtaModal = payload => dispatch({ type: 'NAMTA_MODAL', payload });

    const setIotModalData = payload => dispatch({ type: "IOT_DATA", payload });
    const setWellModalData = payload => dispatch({ type: "WELL_DATA", payload });
    const setRainModalData = payload => dispatch({ type: "RAIN_DATA", payload });
    const setNamtaModalData = payload => dispatch({ type: 'NAMTA_DATA', payload });

    return (
        <ModalContext.Provider value={{showIotModal, showWellModal, showRainModal, showNamtaModal,
            iotModalData, wellModalData, rainModalData, namtaModalData,
            setShowIotModal, setShowWellModal, setShowRainModal, setShowNamtaModal, 
            setIotModalData, setWellModalData, setRainModalData, setNamtaModalData}}>
            {children}
        </ModalContext.Provider>
    )
}