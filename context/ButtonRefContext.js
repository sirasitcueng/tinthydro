import React, { createContext, useEffect, useReducer, useRef, useState } from 'react';

export const ButtonRefContext = createContext({});

const reducer = (state, action) => {
    switch (action.type) {
        case "CHANGE_STATE":
            return {
                ...state, displayState : action.payload,
            }
        default :
            break;
    }
}

export const ButtonRefProvider = ({ children }) => {

    const STATE = {
        STATE_NORMAL : 0,
        STATE_RAIN : 1,
        STATE_SAMPLE : 2,
        STATE_DASHBOARD : 3,
        STATE_SAMPLELOCATION : 4,
        STATE_IMAGE : 5,
    }
    
    const initialState = {
        displayState: STATE.STATE_NORMAL,
    }

    const [state, dispatch] = useReducer(reducer, initialState);
    const setDisplayState = payload => dispatch({ type : 'CHANGE_STATE', payload });
    const { displayState } = state;
    
    // Filter and Search Button
    const btnIotFilter = useRef(null);
    const btnWellFilter = useRef(null);
    const btnRainFilter = useRef(null);
    const btnNamtaFilter = useRef(null);
    const searchBoxRef = useRef(null);

    return (
        <ButtonRefContext.Provider value={{btnIotFilter, btnWellFilter, btnNamtaFilter, searchBoxRef, btnRainFilter, 
            displayState, setDisplayState, STATE}} >
            {children}
        </ButtonRefContext.Provider>
    )
}

export const BtnDefaultContext = createContext({});

export const ButtonDefaultProvider = ({ children }) => {
    const {btnDefault, setBtnDefault} = useState(null);

    return (
        <BtnDefaultContext.Provider value={{btnDefault, setBtnDefault}}>
            {children}
        </BtnDefaultContext.Provider>
    )
}