import React, { createContext, useContext, useEffect, useReducer, useRef } from "react";
import { iotDataExample, namtaDataExample, rainDataExample, wellDataExample } from "./Example";
import { ModalContext } from "./ModalContext";

export const StationContext = createContext({});

const initialState = {
    iotMarker : [], 
    wellMarker : [], 
    namtaMarker : [],
    rainMarker : [],
    iotStation : {}, 
    rainStationData : {},
    wellStationData : {},
    namtaStationData : {},
    serverName : '',
    showedIotStation : [],
    showedRainStation : [],
    showedWellSample : [],
    showedNamtaSample : [],
};

const reducer = (state, action) => {
    switch (action.type) {
        case "SET_IOT_MARKER":
            return {
                ...state, 
                iotMarker : action.payload,
            }
        case "SET_WELL_MARKER":
            return {
                ...state, 
                wellMarker : action.payload,
            }
        case "SET_NAMTA_MARKER":
            return {
                ...state, 
                namtaMarker : action.payload,
            }
        case "SET_RAIN_MARKER":
            return {
                ...state, 
                rainMarker : action.payload,
            }
        case "SET_IOT":
            return {
                ...state, 
                iotStation : action.payload,
            }
        case "SET_WELL":
            return {
                ...state, 
                wellStationData : action.payload,
            }
        case "SET_NAMTA":
            return {
                ...state, 
                namtaStationData : action.payload,
            }
        case "SET_RAIN":
            return {
                ...state, 
                rainStationData : action.payload,
            }
        case "ADD_IOT":
            return {
                ...state, 
                iotStation : {
                    ...state.iotStation, 
                    [action.payload.id] : action.payload.data
                },
            }
        case "ADD_WELL":
            return {
                ...state, 
                wellStationData : {
                    ...state.wellStationData, 
                    [action.payload.id] : action.payload.data
                },
            }
        case "ADD_NAMTA":
            return {
                ...state, 
                namtaStationData : {
                    ...state.namtaStationData, 
                    [action.payload.id] : action.payload.data
                },
            }
        case "ADD_RAIN":
            return {
                ...state, 
                rainStationData : {
                    ...state.rainStationData, 
                    [action.payload.id] : action.payload.data
                },
            }
        case "SET_SHOW_IOT":
            return {
                ...state,
                showedIotStation: action.payload,
            }
        case "SET_SHOW_RAIN":
            return {
                ...state,
                showedRainStation: action.payload,
            }
        case "SET_SHOW_WELL":
            return {
                ...state,
                showedWellSample: action.payload,
            }
        case "SET_SHOW_NAMTA":
            return {
                ...state,
                showedNamtaSample: action.payload,
            }
        case "SET_SERVERNAME":
            return {
                ...state,
                serverName: action.payload,
            }
        default:
            break;
    }
}


export const StationProvider = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState);
    const { 
        iotMarker, wellMarker, namtaMarker, rainMarker, 
        iotStation, rainStationData, wellStationData, namtaStationData, 
        serverName, 
        showedIotStation, showedRainStation, showedWellSample, showedNamtaSample } = state;

    const { setShowIotModal, setIotModalData, setShowWellModal, setShowRainModal, setWellModalData, setRainModalData, setShowNamtaModal, setNamtaModalData  } = useContext(ModalContext);


    const setIotMarker = payload => dispatch({ type: "SET_IOT_MARKER", payload });
    const setWellMarker = payload => dispatch({ type: "SET_WELL_MARKER", payload });
    const setNamtaMarker = payload => dispatch({ type: "SET_NAMTA_MARKER", payload });
    const setRainMarker = payload => dispatch({ type: "SET_RAIN_MARKER", payload });

    const setIotStationData = payload => dispatch({ type: "SET_IOT", payload });
    const setWellStationData = payload => dispatch({ type: "SET_WELL", payload });
    const setNamtaStationData = payload => dispatch({ type: "SET_NAMTA", payload });
    const setRainStationData = payload => dispatch({ type: "SET_RAIN", payload });

    const setShowIotStation = payload => dispatch({ type: "SET_SHOW_IOT", payload });
    const setShowRainStation = payload => dispatch({ type: "SET_SHOW_RAIN", payload });
    const setShowWellSample = payload => dispatch({ type: "SET_SHOW_WELL", payload });
    const setShowNamtaSample = payload => dispatch({ type: "SET_SHOW_NAMTA", payload });

    const addNewStation = payload => {
        const { type, id, data } = payload;
        switch (type) {
            case 'well' :
                dispatch({ type: "ADD_WELL", payload : {id, data} });
                break;
            case 'namta' :
                dispatch({ type: "ADD_NAMTA", payload : {id, data} });
                break;
            case 'rain' :
                dispatch({ type: "ADD_RAIN", payload : {id, data} });
                break;
            default :
                break;
        }
    }

    const resetMarker = (type) => {
        switch (type) {
            case 'iot' :
                iotMarker.forEach(m => m.setMap(null));
                dispatch({ type: "SET_IOT_MARKER", payload : [] });
                break;
            case 'well' :
                wellMarker.forEach(m => m.setMap(null));
                dispatch({ type: "SET_WELL_MARKER", payload : [] });
                break;
            case 'namta' :
                namtaMarker.forEach(m => m.setMap(null));
                dispatch({ type: "SET_NAMTA_MARKER", payload : [] });
                break;
            case 'rain' :
                rainMarker.forEach(m => m.setMap(null));
                dispatch({ type: "SET_RAIN_MARKER", payload : [] });
                break;
            default :
                break;
        }
    }

    const setServerName = payload => dispatch({ type: "SET_SERVERNAME", payload });
    const map = useRef(null);

    useEffect(() => {
        return () => {
            setIotMarker([]);
            setWellMarker([]);
            setNamtaMarker([]);
            setRainMarker([]);
            setIotStationData({});
            setWellStationData({});
            setNamtaStationData({});
            setRainStationData({});
            setShowWellSample([]);
            setShowNamtaSample([]);
        }
    }, []);

    useEffect(() => {
        if (iotStation) addIotMarker(iotStation)
        setShowIotStation(Object.values(iotStation).map(({_id, __v,  isOnline, ...station}) => ({...station, isOnline: isOnline ? 'Online' : 'Offline'})))
    }, [iotStation]);

    useEffect(() => {
        if (rainStationData) addMarker('rain', rainStationData);
        setShowRainStation(Object.values(rainStationData).map(({_id, __v, keyName, ...station}) => ({...station})))
    }, [rainStationData]);

    useEffect(() => {
        if (namtaStationData) addMarker('namta', namtaStationData);
        setShowNamtaSample(
            Object.values(namtaStationData).map(({__v, samplingDate, ...station}) => ({...station}))
            .sort((x, y) => x.sampleCode.localeCompare(y.sampleCode))
        );
    }, [namtaStationData]);

    useEffect(() => {
        if (wellStationData) addMarker('well', wellStationData);
        setShowWellSample(
            Object.values(wellStationData).map(({__v, samplingDate, ...station}) => ({...station}))
            .sort((x, y) => x.sampleCode.localeCompare(y.sampleCode))
        );
    }, [wellStationData]);

    const addIotMarker = (iotStations) => {
        let datas = [];
        for (const IMEI in iotStations) {
            const { lat, lng, siteName, isOnline } = iotStations[IMEI];
            if (lat && lng) {
                const m = new google.maps.Marker({
                    map: map.current,
                    position: { lat, lng },
                    info: iotStations[IMEI],
                    icon: isOnline ? './ICON_IOT_ONLINE.png' : './ICON_IOT_OFFLINE.png',
                    name : siteName,
                    title : siteName,
                })
                m.addListener("click", async () => {
                    let query = await getIotData(IMEI, serverName);//iotDataExample
                    query = query.map(({_id, siteName, IMEI, ...q}) => {
                        return {
                            date : new Date(q.date),
                            depth : q.waterDepth,
                            ec : q.waterConductivity,
                            temp : q.waterTemperature,
                        }
                    })
                    .sort((x, y) => {
                        return x.date > y.date ? 1 : -1;
                    })
                    const modalData = {
                        siteName, isOnline, lat, lng, IMEI,
                        queryData : query,
                    }
                    setIotModalData(modalData);
                    setTimeout(() => setShowIotModal(true), 100); 
                })
                datas = [...datas, m]
            }

        }
        setIotMarker(datas)
    }

    const addMarker = (type, locations) => {
        let datas = [];
        switch (type) {
            case 'rain':
                for (const prov in locations) {
                    const { lat, lng, code } = locations[prov]
                    const m = new google.maps.Marker({
                        map: map.current,
                        position: {lat: parseFloat(lat) , lng: parseFloat(lng) },
                        icon: './ICON_RAIN.png',
                        info : locations[prov],
                        title : code,
                    })
                    m.addListener('click', async () => {
                        const query = await getRainData(prov, serverName);//rainDataExample
                        setRainModalData({...m.info, ...query, camelName : query.name, name : m.info.name});
                        setTimeout(() => setShowRainModal(true), 100);
                    })
                    datas = [...datas, m]
                }
                setRainMarker(datas);
                break;
            case 'namta':
                for (const id in locations) {
                    const { lat, lng, sampleCode, sampleSiteName } = locations[id]
                    const m = new google.maps.Marker({
                        map: map.current,
                        position: {lat: parseFloat(lat) , lng: parseFloat(lng) },
                        icon: './ICON_NAMTA.png',
                        info : locations[id],
                        title : sampleCode,
                        id : id,
                    })
                    m.addListener('click', async () => {
                        const query = await getNamtaData(sampleSiteName, sampleCode, serverName);//namtaDataExample
                        setNamtaModalData({...m.info, query: query});
                        setTimeout(() => setShowNamtaModal(true), 100);
                    })
                    datas = [...datas, m]
                }
                setNamtaMarker(datas)
                break;
            case 'well':
                for (const id in locations) {
                    const { lat, lng, sampleCode, sampleSiteName } = locations[id]
                    const m = new google.maps.Marker({
                        map: map.current,
                        position: {lat: parseFloat(lat) , lng: parseFloat(lng) },
                        icon: './ICON_WELL.png',
                        info : locations[id],
                        title : sampleCode,
                        id : id,
                    })
                    m.addListener('click', async () => {
                        const query = await getWellData(sampleSiteName, sampleCode, serverName);//wellDataExample
                        setWellModalData({...m.info, query: query})
                        setTimeout(() => setShowWellModal(true), 100);
                    })
                    datas = [...datas, m]
                }
                setWellMarker(datas)
                break;
            default:
                break;
        }
    }

    return (
        <StationContext.Provider value={{
            iotStation, rainStationData, wellStationData, namtaStationData, 
            setIotStationData, setWellStationData, setNamtaStationData, setRainStationData,
            map, iotMarker, wellMarker, namtaMarker, rainMarker, setWellMarker, setNamtaMarker, addNewStation, resetMarker,
            serverName, setServerName,
            showedIotStation, showedRainStation, setShowIotStation, setShowRainStation,
            showedWellSample, showedNamtaSample, setShowWellSample, setShowNamtaSample,
        }}>
            {children}
        </StationContext.Provider>
    )
}

const getRainData = async (prov, serverName) => {
    const rawData = await fetch(`${serverName}api/rainData`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body : JSON.stringify({
            prov : prov,
        })
    });
    const jsonData = await rawData.json();
    return jsonData;
}

const getIotData = async (IMEI, serverName) => {
    const rawData = await fetch(`${serverName}api/iotData`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body : JSON.stringify({
            IMEI : IMEI,
        })
    });
    const jsonData = await rawData.json();
    if (jsonData) return Object.values(jsonData) ;
}

const getNamtaData = async (sampleSiteName, sampleCode, serverName) => {
    const searchParams = new URLSearchParams({
        sampleSiteName,
        sampleCode,
    })
    const rawData = await fetch(`${serverName}api/namtaData?${searchParams}`, {
        method: 'GET',
    });
    const jsonData = await rawData.json();
    if (jsonData) return jsonData;
}

const getWellData = async (sampleSiteName, sampleCode, serverName) => {
    const searchParams = new URLSearchParams({
        sampleSiteName,
        sampleCode,
    })
    const rawData = await fetch(`${serverName}api/wellData?${searchParams}`, {
        method: 'GET',
    });
    const jsonData = await rawData.json();
    if (jsonData) return jsonData;
}
