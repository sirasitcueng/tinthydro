export const iotExample = {
    "869405031267017" : {
        "_id": "61e8e10687a94705e676d650",
        "IMEI": "869405031267017",
        "siteName": "Site I",
        "lat": 15.87,
        "lng": 100.9925,
        "isOnline": true,
        "__v": 0
    }
}

export const rainExample = {
    "nan" : {
        "_id": "61dfe0a9c9e1b5d80bb5670b",
        "name": "Nan",
        "lat": 18.766894,
        "lng": 100.763492,
        "code": "NAN",
        "__v": 0,
        "keyName": "nan"
    }
}

export const namtaExample = {
    "620ce8b253a7433d9476b655": {
        "_id": "620ce8b253a7433d9476b655",
        "projectNumber": "THA 8015",
        "sampleCode": "PL 51-55",
        "sampleSiteName": "จ.พิษณุโลก",
        "lat": "16.27085667",
        "lng": "100.2153236",
        "developingDepth": "",
        "screenDepth": "",
        "altitude": "",
        "aquiferName": "",
        "__v": 0
    }
}

export const wellExample = {
    "620ce1f953a7433d9476b623": {
        "_id": "620ce1f953a7433d9476b623",
        "projectNumber": "xx",
        "sampleCode": "CM1",
        "sampleSiteName": "Wat Mai San Thung",
        "lat": "18.4503889",
        "lng": "98.69558964",
        "developingDepth": "192",
        "screenDepth": "175-187",
        "altitude": "289",
        "aquiferName": "High terrace/Chaing Mai (Qht/Qcm) ",
        "__v": 0
    }
}

export const iotDataExample = [
    {
        "_id": "61e8e569bdd2e4542aa587f4",
        "siteName": "Site I",
        "date": "2022-01-20T04:30:33.664Z",
        "IMEI": "869405031267017",
        "waterDepth": 0,
        "waterConductivity": 3.7,
        "waterTemperature": 30
    },
    {
        "_id": "61e8f35cbdd2e4542aa587f6",
        "siteName": "Site I",
        "date": "2022-01-20T05:30:04.212Z",
        "IMEI": "869405031267017",
        "waterDepth": 0,
        "waterConductivity": 3.7,
        "waterTemperature": 30
    },
    {
        "_id": "61e9017cbdd2e4542aa587f8",
        "siteName": "Site I",
        "date": "2022-01-20T06:30:20.850Z",
        "IMEI": "869405031267017",
        "waterDepth": 0,
        "waterConductivity": 3.7,
        "waterTemperature": 30
    },
    {
        "_id": "61e90f98bdd2e4542aa587fa",
        "siteName": "Site I",
        "date": "2022-01-20T07:30:32.513Z",
        "IMEI": "869405031267017",
        "waterDepth": 0,
        "waterConductivity": 3.7,
        "waterTemperature": 30
    },
    {
        "_id": "61e91dadbdd2e4542aa587fb",
        "siteName": "Site I",
        "date": "2022-01-20T08:30:37.491Z",
        "IMEI": "869405031267017",
        "waterDepth": 0,
        "waterConductivity": 3.71,
        "waterTemperature": 30
    },
    {
        "_id": "61e92babbdd2e4542aa587fc",
        "siteName": "Site I",
        "date": "2022-01-20T09:30:19.579Z",
        "IMEI": "869405031267017",
        "waterDepth": 0,
        "waterConductivity": 3.71,
        "waterTemperature": 30
    },
    {
        "_id": "61e939d7bdd2e4542aa587fd",
        "siteName": "Site I",
        "date": "2022-01-20T10:30:47.089Z",
        "IMEI": "869405031267017",
        "waterDepth": 0,
        "waterConductivity": 3.71,
        "waterTemperature": 30
    },
    {
        "_id": "61e955cebdd2e4542aa587fe",
        "siteName": "Site I",
        "date": "2022-01-20T12:30:06.131Z",
        "IMEI": "869405031267017",
        "waterDepth": 0,
        "waterConductivity": 3.71,
        "waterTemperature": 30
    },
    {
        "_id": "61e963e9bdd2e4542aa587ff",
        "siteName": "Site I",
        "date": "2022-01-20T13:30:17.659Z",
        "IMEI": "869405031267017",
        "waterDepth": 0,
        "waterConductivity": 3.71,
        "waterTemperature": 30
    },
    {
        "_id": "61e97204bdd2e4542aa58800",
        "siteName": "Site I",
        "date": "2022-01-20T14:30:28.530Z",
        "IMEI": "869405031267017",
        "waterDepth": 0,
        "waterConductivity": 3.71,
        "waterTemperature": 30
    },
    {
        "_id": "61e98025bdd2e4542aa58801",
        "siteName": "Site I",
        "date": "2022-01-20T15:30:45.167Z",
        "IMEI": "869405031267017",
        "waterDepth": 0,
        "waterConductivity": 3.71,
        "waterTemperature": 30
    },
    {
        "_id": "61e98e31bdd2e4542aa58802",
        "siteName": "Site I",
        "date": "2022-01-20T16:30:41.338Z",
        "IMEI": "869405031267017",
        "waterDepth": 0,
        "waterConductivity": 3.71,
        "waterTemperature": 30
    },
    {
        "_id": "61e99c48bdd2e4542aa58803",
        "siteName": "Site I",
        "date": "2022-01-20T17:30:48.367Z",
        "IMEI": "869405031267017",
        "waterDepth": 0,
        "waterConductivity": 3.71,
        "waterTemperature": 30
    },
    {
        "_id": "61e9aa28bdd2e4542aa58804",
        "siteName": "Site I",
        "date": "2022-01-20T18:30:00.372Z",
        "IMEI": "869405031267017",
        "waterDepth": 0,
        "waterConductivity": 3.71,
        "waterTemperature": 30
    },
    {
        "_id": "61e9b84fbdd2e4542aa58805",
        "siteName": "Site I",
        "date": "2022-01-20T19:30:23.419Z",
        "IMEI": "869405031267017",
        "waterDepth": 0,
        "waterConductivity": 3.71,
        "waterTemperature": 30
    },
    {
        "_id": "61e9c650bdd2e4542aa58806",
        "siteName": "Site I",
        "date": "2022-01-20T20:30:08.700Z",
        "IMEI": "869405031267017",
        "waterDepth": 0,
        "waterConductivity": 3.71,
        "waterTemperature": 30
    },
    {
        "_id": "61e9d489bdd2e4542aa58807",
        "siteName": "Site I",
        "date": "2022-01-20T21:30:49.114Z",
        "IMEI": "869405031267017",
        "waterDepth": 0,
        "waterConductivity": 3.71,
        "waterTemperature": 30
    },
    {
        "_id": "61e9fe96bdd2e4542aa58808",
        "siteName": "Site I",
        "date": "2022-01-21T00:30:14.447Z",
        "IMEI": "869405031267017",
        "waterDepth": 0,
        "waterConductivity": 3.71,
        "waterTemperature": 30
    },
    {
        "_id": "61ea0cb0bdd2e4542aa58809",
        "siteName": "Site I",
        "date": "2022-01-21T01:30:24.049Z",
        "IMEI": "869405031267017",
        "waterDepth": 0,
        "waterConductivity": 3.71,
        "waterTemperature": 30
    },
    {
        "_id": "61ea1ad2bdd2e4542aa5880a",
        "siteName": "Site I",
        "date": "2022-01-21T02:30:42.608Z",
        "IMEI": "869405031267017",
        "waterDepth": 0,
        "waterConductivity": 3.71,
        "waterTemperature": 30
    },
    {
        "_id": "61ea28debdd2e4542aa5880b",
        "siteName": "Site I",
        "date": "2022-01-21T03:30:38.231Z",
        "IMEI": "869405031267017",
        "waterDepth": 0,
        "waterConductivity": 3.71,
        "waterTemperature": 30
    },
    {
        "_id": "61ea36eebdd2e4542aa5880c",
        "siteName": "Site I",
        "date": "2022-01-21T04:30:38.776Z",
        "IMEI": "869405031267017",
        "waterDepth": 0,
        "waterConductivity": 3.71,
        "waterTemperature": 30
    },
    {
        "_id": "61ea44e7bdd2e4542aa5880d",
        "siteName": "Site I",
        "date": "2022-01-21T05:30:15.786Z",
        "IMEI": "869405031267017",
        "waterDepth": 0,
        "waterConductivity": 3.71,
        "waterTemperature": 30
    },
    {
        "_id": "61ea52f8bdd2e4542aa5880e",
        "siteName": "Site I",
        "date": "2022-01-21T06:30:16.378Z",
        "IMEI": "869405031267017",
        "waterDepth": 0,
        "waterConductivity": 3.72,
        "waterTemperature": 30
    }
]

export const wellDataExample = [
    {
        "_id": "620ce31153a7433d9476b62d",
        "sampleCode": "CM1",
        "sampleSiteName": "Wat Mai San Thung",
        "samplingDate": "14/08/07",
        "waterTable": "9.76",
        "pH": "7.00",
        "ec": "694",
        "temperature": "21.70",
        "do": "",
        "orp": "",
        "alkalinity": "",
        "ca": "14",
        "mg": "0.4",
        "k": "2",
        "na": "100",
        "fe": "0.1",
        "mn": "0",
        "hco3": "283",
        "co3": "27",
        "cl": "2.4",
        "no3": "<0.9",
        "so4": "<1",
        "siO2": "",
        "o18": "",
        "h2": "",
        "h3": "2.1",
        "c13Vpbd": "-10.62",
        "c14Pmc": "",
        "c14Year": "\r",
        "__v": 0
    }
]

export const namtaDataExample = [
    {
        "_id": "620ce8c253a7433d9476b657",
        "sampleCode": "PL 51-55",
        "sampleSiteName": "จ.พิษณุโลก",
        "samplingDate": "25/01/07",
        "pH": "7.2",
        "ec": "155",
        "temperature": "27.3",
        "do": "",
        "orp": "",
        "alkalinity": "",
        "ca": "22",
        "mg": "6.9",
        "k": "0.9",
        "na": "< 4",
        "fe": "0.6",
        "mn": "0",
        "hco3": "98",
        "co3": "0",
        "cl": "2.8",
        "no3": "1.7",
        "so4": "3",
        "siO2": "",
        "o18": "-6.56",
        "h2": "-48.11",
        "h3": "",
        "c13Vpbd": "",
        "c14Pmc": "",
        "c14Year": "",
        "__v": 0
    },
    {
        "_id": "620ce8ce53a7433d9476b659",
        "sampleCode": "PL 51-55",
        "sampleSiteName": "จ.พิษณุโลก",
        "samplingDate": "26/01/07",
        "pH": "6.8",
        "ec": "219",
        "temperature": "26.3",
        "do": "",
        "orp": "",
        "alkalinity": "",
        "ca": "21",
        "mg": "15",
        "k": "2.4",
        "na": "7",
        "fe": "1.5",
        "mn": "0.9",
        "hco3": "109",
        "co3": "0",
        "cl": "8.8",
        "no3": "10",
        "so4": "12",
        "siO2": "",
        "o18": "-6.49",
        "h2": "-46.95",
        "h3": "",
        "c13Vpbd": "",
        "c14Pmc": "",
        "c14Year": "",
        "__v": 0
    },
    {
        "_id": "620ce8d653a7433d9476b65b",
        "sampleCode": "PL 51-55",
        "sampleSiteName": "จ.พิษณุโลก",
        "samplingDate": "26/01/07",
        "pH": "7.6",
        "ec": "162",
        "temperature": "27.1",
        "do": "",
        "orp": "",
        "alkalinity": "",
        "ca": "22",
        "mg": "11",
        "k": "0.4",
        "na": "< 4",
        "fe": "2.8",
        "mn": "0.1",
        "hco3": "108",
        "co3": "0",
        "cl": "2",
        "no3": "< 0.9",
        "so4": "3",
        "siO2": "",
        "o18": "-6.47",
        "h2": "-46.94",
        "h3": "",
        "c13Vpbd": "",
        "c14Pmc": "",
        "c14Year": "",
        "__v": 0
    }
]

export const rainDataExample = {
    "_id": "61ab1a7cd2ecf95f876c068d",
    "name": "nan",
    "precipitation": {
        "2020-10": {
            "3": 2.2,
            "4": 0.2,
            "15": 0.5,
            "16": 1.6,
            "18": 0.2,
            "26": 4.4,
            "27": 10.1,
            "29": 2,
            "30": 8,
            "31": 0.5,
            "32": null,
            "33": null,
            "34": null
        },
        "2020-09": {
            "1": 8,
            "3": 1.2,
            "4": 6.8,
            "7": 13.1,
            "8": 1.9,
            "9": 0.1,
            "11": 3.4,
            "14": 17.7,
            "17": 0.2,
            "18": 5.2,
            "19": 14.8,
            "20": 10.1,
            "21": 3.1,
            "22": 0.3,
            "23": 35.1,
            "24": 2.3,
            "26": 1.8,
            "27": 0.3,
            "28": 0.2,
            "29": 2.4
        }
    },
    "__v": 0,
    "h2": {
        "2020-10": {
            "3": -70.24,
            "4": -75.67,
            "15": -60.91,
            "16": -100.49,
            "18": -20.44,
            "26": -37.91,
            "27": -40.16,
            "29": -44.05,
            "30": -55.57,
            "31": -33.62,
            "32": null,
            "33": null
        },
        "2020-09": {
            "1": -33.62,
            "3": -41.85,
            "4": -45.09,
            "7": -44.16,
            "8": -47.35,
            "9": -45.84,
            "11": -63.38,
            "14": -61.21,
            "17": -50.02,
            "18": -58.94,
            "19": -98.67,
            "20": -112.51,
            "21": -84.93,
            "22": -60.77,
            "23": -53.68,
            "24": -98.16,
            "26": -57.31,
            "27": -55.65,
            "28": -58.89,
            "29": -64.47,
            "31": null,
            "32": null,
            "33": null
        }
    },
    "o18": {
        "2020-10": {
            "3": -10.12,
            "4": -10.62,
            "15": -8.6,
            "16": -13.62,
            "18": -0.11,
            "26": -6.2,
            "27": -6.55,
            "29": -5.81,
            "30": -8.32,
            "31": -4.92,
            "32": null,
            "33": null,
            "34": null
        },
        "2020-09": {
            "1": -5.07,
            "3": -6.63,
            "4": -7.1,
            "7": -7.66,
            "8": -8.12,
            "9": -6.29,
            "11": -9.25,
            "14": -9.64,
            "17": -7.37,
            "18": -8.82,
            "19": -13.69,
            "20": -15.56,
            "21": -11.17,
            "22": -8.39,
            "23": -7.56,
            "24": -13.37,
            "26": -8.01,
            "27": -7.77,
            "28": -8.02,
            "29": -9.5,
            "31": null,
            "32": null,
            "33": null
        }
    }
}

