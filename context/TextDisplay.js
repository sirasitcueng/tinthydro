export const TXT = {
    HEADER_TITLE_MOBILE : {
        en : 'Isotope Hydrology',
        th : 'ข้อมูลไอโซโทปไฮโดรโลยี',
    },
    HEADER_TITLE : {
        en : 'Thailand Database of Isotope Hydrology',
        th : 'ฐานข้อมูลไอโซโทป ไฮโดรโลยีของประเทศไทย',
    },
    IOT_TAB : {
        en : 'Groundwater monitoring wells',
        th : 'สถานีตรวจติดตามน้ำบาดาล',
    },
    WELL_TAB : {
        en : 'Groundwater samples',
        th : 'ตัวอย่างน้ำบาดาล',
    },
    RAIN_TAB : {
        en : 'Rainfall stations',
        th : 'สถานีเก็บตัวอย่างน้ำฝน',
    },
    NAMTA_TAB : {
        en : 'River samples',
        th : 'ตัวอย่างน้ำท่า',
    },
    LOG_IN : {
        en : 'Sign In',
        th : 'ลงชื่อเข้าใช้',
    },
    SIGN_IN : {
        en : 'Sign In',
        th : 'เข้าสู่ระบบ',
    },
    ADD_DATA : {
        en : 'Add Data',
        th : 'เพิ่มข้อมูล',
    },
    UPDATE_DATA : {
        en : 'Update Data',
        th : 'อัพเดทข้อมูล',
    },
    DELETE_DATA : {
        en : 'Delete Data',
        th : 'ลบข้อมูล',
    },
    DELETE_IMAGE : {
        en : 'Delete Image',
        th : 'ลบรูปภาพ',
    },
    CLOSE : {
        en : 'Close',
        th : 'ปิด'
    },
    ADD_LOCATION : {
        en : '+ New Location',
        th : '+ ตำแหน่งใหม่',
    },
    ADD_RAIN : {
        en : '+ Rain',
        th : '+ น้ำฝน',
    },
    ADD_SAMPLE : {
        en : '+ Sample Water',
        th : '+ ตัวอย่างน้ำ',
    },
    ADD_IMAGE : {
        en : '+ Image',
        th : '+ รูปภาพ',
    },
    DASHBOARD : {
        en : 'Dashboard',
        th : 'แดชบอร์ด',
    },
    SIGN_OUT : {
        en : 'Sign Out',
        th : 'ออกจากระบบ',
    },
    EMAIL : {
        en : 'Email',
        th : 'อีเมล',
    },
    PASSWORD : {
        en : 'Password',
        th : 'รหัสผ่าน',
    },
    PLEASE_FILL : {
        en : 'Please fill your ',
        th : 'กรุณาใส่'
    },
    SUCCESS : {
        en : ' successfully',
        th : 'สำเร็จ'
    },
    PASTE : {
        en : 'paste',
        th : 'วาง'
    },
    CLEAR : {
        en : 'clear',
        th : 'ล้าง'
    },
}