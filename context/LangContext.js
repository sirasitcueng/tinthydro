import React, { createContext, useEffect, useReducer } from "react";

export const LangContext = createContext({});

const initialState = {
    lang : 'th'
};

const reducer = (state, action) => {
    switch(action.type) {
        case "SET_LANG":
            return {
                ...state, 
                lang : action.payload,
            }
        default:
            break;
    }
}


export const LangProvider = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState);
    const { lang } = state;
    const setLang = payload => dispatch({ type: "SET_LANG", payload });

    useEffect(() => {
        return () => {
            setLang('th');
        }
    }, []);

    return (
        <LangContext.Provider value={{
            lang, setLang, 
        }}>
            {children}
        </LangContext.Provider>
    )
}
