import ConnectDB from "../../middleware/ConnectDB";
import { NamtaStationModel } from "../../middleware/namta/NamtaStationData";

const handler = async(req, res) => {
    if (req.method === 'GET') {
        try {
            const u = await NamtaStationModel.find();
            return res.status(200).send(u);

        }
        catch (e) {
            return res.status(500).send(e.message);
        }
    }
    else if (req.method === 'POST') {
        try {
            const data = new NamtaStationModel(req.body);
            let savedData = await data.save();
            const id = savedData.id
            return res.status(200).send({id : id})
        }
        catch (e) {
            return res.status(500).send(e.message);
        }
    }
    else if (req.method === 'DELETE') {
        try {
            const data = await NamtaStationModel.findByIdAndDelete(req.body.id);
            return res.status(200).send(data);
        }
        catch (e) {
            return res.status(500).send(e.message);
        }
    }
    else {
        res.status(422).send('error');
    }
}

export default ConnectDB(handler);


