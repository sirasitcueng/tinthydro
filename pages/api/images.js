import ConnectDB from "../../middleware/ConnectDB";
import { ImageSchemaModel } from "../../middleware/img/ImageModel";

const handler = async(req, res) => {

    try {
        switch (req.method) {
            case 'GET':
                const { sampleSiteName, sampleCode, type } = req.query;
                if (sampleSiteName && sampleCode && type) {
                    const getData = await ImageSchemaModel.find({ sampleCode, sampleSiteName, type });
                    return res.status(200).send(getData);
                }
                else {
                    const allData = await ImageSchemaModel.find();
                    return res.status(200).send(allData);
                }
            case 'POST':
                const buildData = await ImageSchemaModel(req.body);
                let savedData = await buildData.save();
                let savedId = savedData.id
                return res.status(200).send({id : savedId})
            case 'DELETE':
                const { id } = req.body;
                if (!id) throw('missing parameter')
                const deletedData = await ImageSchemaModel.findByIdAndDelete(id)
                return res.status(200).send(deletedData);
            default:
                return res.status(422).send('error');
        }
    }
    catch (e) {
        return res.status(500).send(e.message);
    }
}

export default ConnectDB(handler);


