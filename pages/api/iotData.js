import ConnectDB from "../../middleware/ConnectDB";
import { IotDataModel } from "../../middleware/iot/IotData";
import { IotStationModel } from '../../middleware/iot/IotStationData';

const handler = async (req, res) => {
    try {
        const { IMEI } = req.body;
        switch (req.method) {
            case 'GET' :
                const u = await IotDataModel.find();
                return res.status(200).send(u);
            case 'POST':
                if (!IMEI) throw 'missing parameter'
                const data1 = await IotDataModel.find({ IMEI: IMEI });
                return res.status(200).send(data1);
            /*case 'UPDATE':
                const date = new Date().setHours(new Date().getHours() - 3);
                const stations = await IotStationModel.find();
                let imeiArray = [];
                const stationsJsonData = await stations.json();
                console.log(stationsJsonData);
                if (stationsJsonData) {
                    stationsJsonData.forEach(async d => {
                        imeiArray = [...imeiArray, d.IMEI]
                        let finded = await IotDataModel.findOne({
                            date : { $gte : date },
                            IMEI : d.IMEI,
                        });
                        await IotStationModel.findOneAndUpdate({IMEI : d.IMEI }, {$set : {isOnline : !!finded}});
                        console.log(`${d.siteName} : ${!!finded}`);
                    })
                    return res.status(200).send(true);
                }
                else {
                    return res.status(422).send(false);
                }*/
            case 'PUT':
                const { date, siteName, waterDepth, waterConductivity, waterTemperature } = req.body
                const data2 = await IotDataModel.create({date, IMEI, siteName, waterDepth, waterConductivity, waterTemperature})
                return res.status(200).send(data2);
            default:
                return res.status(422).send('error');
        }
    }
    catch (e) {
        return res.status(500).send(e.message);
    }
}

export default ConnectDB(handler);