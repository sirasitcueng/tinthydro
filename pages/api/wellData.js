import ConnectDB from "../../middleware/ConnectDB";
import { WellDataModel } from "../../middleware/well/WellData";

const handler = async(req, res) => {

    const { id, update } = req.body;

    try {
        switch (req.method) {
            case 'GET':
                const { sampleSiteName, sampleCode } = req.query;
                if (sampleSiteName && sampleCode) {
                    const getData = await WellDataModel.find({ sampleCode, sampleSiteName });
                    return res.status(200).send(getData);
                }
                else {
                    const allData = await WellDataModel.find();
                    return res.status(200).send(allData);
                }
                
            case 'POST':
                const buildData = await WellDataModel(req.body);
                let savedData = await buildData.save();
                let savedId = savedData.id
                return res.status(200).send({id : savedId})
                
            case 'PUT':
                if (!id && !update) throw('missing parameter')
                const updatedData = await WellDataModel.findByIdAndUpdate(id, { $set:  update});
                return res.status(200).send(updatedData);
            case 'DELETE':
                if (!id) throw('missing parameter')
                const deletedData = await WellDataModel.findByIdAndDelete(id)
                return res.status(200).send(deletedData);
            default:
                return res.status(422).send('error');
        }
    }
    catch (e) {
        return res.status(500).send(e.message);
    }
}

export default ConnectDB(handler);


