import ConnectDB from "../../middleware/ConnectDB";
import { VisitorModel } from '../../middleware/others/VisitorModel'

const handler = async(req, res) => {
    try {
        switch (req.method) {
            case 'GET':
                const data = await VisitorModel.find();
                return res.status(200).send(data);
            case 'POST':
                const data2 = await VisitorModel.findOneAndUpdate({}, {$inc : {'totalVisit' : 1}});
                return res.status(200).send(data2);
            default:
                return res.status(422).send('error');
        }
    }
    catch (e) {
        return res.status(500).send(e.message);
    }
}

export default ConnectDB(handler);