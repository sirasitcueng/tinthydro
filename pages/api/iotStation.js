import ConnectDB from "../../middleware/ConnectDB";
import { IotDataModel } from "../../middleware/iot/IotData";
import { IotStationModel } from "../../middleware/iot/IotStationData";

const handler = async(req, res) => {
    try {
        switch (req.method) {
            case 'GET' :
                const u = await IotStationModel.find();
                const date = new Date().setHours(new Date().getHours() - 3);
                if (u) {
                    u.forEach(async (d, i) => {
                        let finded = await IotDataModel.findOne({
                            date : { $gte : date },
                            IMEI : d.IMEI,
                        });
                        //await IotStationModel.findOneAndUpdate({IMEI : d.IMEI }, {$set : {isOnline : !!finded}});
                        u[i].isOnline = !!finded;
                    })
                    return res.status(200).send(u);
                }
                else {
                    return res.status(422).send('get error');
                }
                
            case 'POST' :
                const data = new IotStationModel(req.body);
                let savedData = await data.save();
                const id = savedData.id
                return res.status(200).send({id : id})
            case 'PUT':
                const { siteName, lat, lng, IMEI } = req.body
                const data2 = await IotStationModel.findOneAndUpdate({IMEI}, {$set : {siteName, lat, lng}})
                return res.status(200).send(data2);
            default:
                return res.status(422).send('error');
        }
    }
    catch (e) {
        return res.status(500).send(e.message);
    }
}

export default ConnectDB(handler);