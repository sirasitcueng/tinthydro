import ConnectDB from "../../middleware/ConnectDB";
import { WellStationModel } from "../../middleware/well/WellStationData";

const handler = async(req, res) => {
    if (req.method === 'GET') {
        try {
            const u = await WellStationModel.find();
            return res.status(200).send(u);

        }
        catch (e) {
            return res.status(500).send(e.message);
        }
    }
    else if (req.method === 'POST') {
        try {
            const data = new WellStationModel(req.body);
            let savedData = await data.save();
            const id = savedData.id
            return res.status(200).send({id : id})
        }
        catch (e) {
            return res.status(500).send(e.message);
        }
    }
    else if (req.method === 'DELETE') {
        try {
            const data = await WellStationModel.findByIdAndDelete(req.body.id);
            return res.status(200).send(data);
        }
        catch (e) {
            return res.status(500).send(e.message);
        }
    }
    else {
        res.status(422).send('error');
    }
}

export default ConnectDB(handler);


