import ConnectDB from "../../middleware/ConnectDB";
import { RainDataModel } from "../../middleware/rain/RainData";

const handler = async(req, res) => {
    try {
        const { prov, type, dateString, dateObj } = req.body;
        switch (req.method) {
            case 'POST':
                if (!prov) throw 'missing parameter'
                const data1 = await RainDataModel.findOne({ name: prov });
                return res.status(200).send(data1);
            case 'PUT':
                if (!(prov || type || dateString || dateObj)) throw 'missing parameter';
                const data2 = await RainDataModel.find({name : prov});
                if (data2.length) {
                    const updatedData = await RainDataModel.findOneAndUpdate({name : prov}, { $set:  {[`${type}.${dateString}`] : dateObj}})
                    return res.status(200).send(updatedData);
                }
                else {
                    const d = {
                        name : prov,
                        [type] : {
                            [dateString] : dateObj,
                        }
                    }
                    const newData = new RainDataModel(d)
                    const savedData = await newData.save();
                    return res.status(200).send(savedData);
                }
            case 'DELETE':
                if (!prov && !type && !dateString) throw 'missing parameter';
                if (prov === 'all') {
                    const updatedData = await RainDataModel.updateMany({}, { $unset:  {[`${type}.${dateString}`] : {}}});
                    return res.status(200).send(updatedData);
                }
                else {
                    const dbData = await RainDataModel.find({name : prov, [type] : dateString});
                    console.log(dbData, type, dateString)
                    const updatedData = await RainDataModel.updateOne({name : prov}, { $unset:  {[`${type}.${dateString}`] : {}}})
                    return res.status(200).send(updatedData);
                }
            default:
                return res.status(422).send('error');
        }
    }
    catch (e) {
        return res.status(500).send(e.message);
    }
}

export default ConnectDB(handler);


