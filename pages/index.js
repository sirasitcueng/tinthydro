import React,{ useEffect, useState } from 'react';
import Layout from '../components/Layout';
import GooMap from '../components/GooMap';
import HeaderPublic from '../components/HeaderComponents/HeaderPublic';
import Header from '../components/HeaderComponents/Header';
import { auth } from "../config/firebase";
import SubHeader from '../components/HeaderComponents/SubHeader';
import {SSRProvider} from '@react-aria/ssr';
import { onAuthStateChanged } from 'firebase/auth';
import LangHeader from '../components/HeaderComponents/LangHeader';


export const getStaticProps = async () => {
    const serverName = process.env.SERVER_NAME;
    //const serverName = `http://localhost:3000/`;
    //const serverName = "https://tinthydro.tint.or.th/"
    /*let wellStationData = {};
    let rainStationData = {};
    let namtaStationData = {};
    let iotStation = {};
    let visitors;

    try {
        iotStation = await getIotStation(serverName);
    }
    catch (e) {
        console.log(`FETCH IOT DATA ERROR: ${e}`);
    }

    try {
        wellStationData = await getWellStation(serverName);
    }
    catch (e) {
        console.log(`FETCH WELL DATA ERROR: ${e}`);
    }

    try {
        rainStationData = await getRainStation(serverName);
    }
    catch (e) {
        console.log(`FETCH RAIN STATION ERROR: ${e}`);
    }

    try {
        namtaStationData = await getNamtaStation(serverName)
    }
    catch (e) {
        console.log(`FETCH NAMTA DATA ERROR: ${e}`);
    }

    try {
        visitors = await getVisitorData(serverName);
    }
    catch (e) {
        console.log(e)
    }

    wellStationData = wellStationData ||{};
    rainStationData = rainStationData || {};
    namtaStationData = namtaStationData || {};
    iotStation = iotStation || {};
    visitors || 0;*/

    return {
        props: { serverName }
    }
}


const Index = ({ serverName }) => {
    
    const [isLogin, setLogin] = useState();

    useEffect(() => {
        onAuthStateChanged(auth, user => {
            user ? setLogin(true) : setLogin(false)
        })
        return () => {
            setLogin();
        }
    }, []);

    return (
        <div className='main-container'>
        <Layout><SSRProvider>
            <LangHeader />
            {
                isLogin === true ? (
                    <Header auth={auth} />
                ) : isLogin === false ? (
                    <HeaderPublic  />
                ) : (
                    <div />
                )
            }
            <SubHeader auth={auth} isLogin={isLogin} />

            <div className="container-fluid index-container">
                <GooMap h={700} serverName={serverName} isLogin={isLogin} />
            </div>
        </SSRProvider></Layout>
        </div>
    )
}
export default Index;

/*const getIotStation = async (serverName) => {
    let iotStation = {};
    const rawData = await fetch(`${serverName}api/iotStation`);
    const jsonData = await rawData.json();
    if (jsonData) jsonData.forEach(d => {
        iotStation[d.IMEI] = d;
    });
    return iotStation;
}


const getRainStation = async (serverName) => {
    const wordToCamel = (wd) => {
        const result = wd.replace(/ /g, '')
        return result.charAt(0).toLowerCase() + result.slice(1);
    }
    let data = {};
    const rawData = await fetch(`${serverName}api/rainStation`, {
        method: 'GET',
    })
    const jsonData = await rawData.json();
    if (jsonData) jsonData.forEach(d => {
        data[wordToCamel(d.name)] = {...d, keyName : wordToCamel(d.name)};
    })
    return data;
}

const getWellStation = async (serverName) => {
    let data = {};
    const rawData = await fetch(`${serverName}api/wellStation`, {
        method: 'GET',
    })
    const jsonData = await rawData.json();
    if (jsonData) jsonData.forEach(d => {
        data[d._id] = d;
    })
    return data;
}

const getNamtaStation = async (serverName) => {
    let data = {};
    const rawData = await fetch(`${serverName}api/namtaStation`, {
        method: 'GET',
    })
    const jsonData = await rawData.json();
    if (jsonData) jsonData.forEach(d => {
        data[d._id] = d;
    })
    return data;
    
}

const getVisitorData = async (serverName) => {
    const rawData = await fetch(`${serverName}api/visitors`, {
        method: 'POST',
    })
    const jsonData = await rawData.json();
    if (Array.isArray(jsonData)) {
        return jsonData[0]['totalVisit']
    }
    else if (typeof jsonData === 'object') {
        return jsonData['totalVisit']
    }
    else {
        return {}
    }
    //return jsonData[0]['totalVisit'];
}*/



