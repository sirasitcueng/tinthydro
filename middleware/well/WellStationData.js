import { model, models, Schema} from 'mongoose';
import { stationDataSchemaObj } from '../../context/SampleField';

models = {};

/*const wellStationSchema = new Schema({
    sampleCode : String,
    sampleSiteName : String,
    lat : String,
    lng : String,
    developingDepth : String,
    screenDepth : String,
    altitude : String,
    aquiferName : String,
})*/

const wellStationSchema = new Schema(stationDataSchemaObj);

export const WellStationModel = model('wellstations', wellStationSchema)