import { model, models, Schema} from 'mongoose';

const imageSchema = new Schema({
    type : String,
    sampleSiteName : String,
    sampleCode : String,
    imgName : String,
    imgDesc : String,
    img : {
        data: Buffer,
        contentType: String,
    }
})

models = {};

export const ImageSchemaModel = model('images', imageSchema);