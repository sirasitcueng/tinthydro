import { model, models, Schema} from 'mongoose';

models = {};

const visitorSchema = new Schema({
    totalVisit : Number,
})

export const VisitorModel = model('visitors', visitorSchema)