import { model, models, Schema} from 'mongoose';

models = {};

const rainStationSchema = new Schema({
    name : String,
    o18 : {},
    h2 : {},
    precipitation : {},
})

export const RainDataModel = model('data3', rainStationSchema)