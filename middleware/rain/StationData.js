import { model, models, Schema} from 'mongoose';

models = {};

const rainStationSchema = new Schema({
    name : String,
    lat: Number,
    lng: Number,
    code: String,
})

export const RainStationModel = model('rainStation0', rainStationSchema)