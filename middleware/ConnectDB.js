import mongoose from "mongoose"

//const mongoURI = `mongodb+srv://root:Adminardt01@cluster1.e7wtz.mongodb.net/db?retryWrites=true&w=majority`;
//const mongoURI = `mongodb://127.0.0.1:27017/db`;
const mongoURI = `mongodb://hydro:hd%40TINT997@103.125.82.186/db?retryWrites=true&w=majority`

const ConnectDB = handler => async (req, res) => {
    if (mongoose.connections[0].readyState) {
        return handler(req, res)
    }

    await mongoose.connect(mongoURI, {
        useUnifiedTopology: true,
        useNewUrlParser: true,
    });
    return handler(req, res);
    
}

export default ConnectDB;