import { model, models, Schema} from 'mongoose';

models = {};

const iotStationSchema = new Schema({
    IMEI : String,
    siteName : String,
    lat : Number,
    lng : Number,
    isOnline : Boolean,
})

export const IotStationModel = model('iotStations', iotStationSchema);