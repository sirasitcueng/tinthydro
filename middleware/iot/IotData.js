import { model, models, Schema} from 'mongoose';

const iotSchema = new Schema({
    siteName: String,
    date: Date,
    level: Number,
    IMEI : String,
    waterDepth : Number,
    waterConductivity : Number,
    waterTemperature : Number,
})

models = {};

export const IotDataModel = model('iotdatas', iotSchema);