import { model, models, Schema} from 'mongoose';
import { namtaDataSchemaObj } from '../../context/SampleField';

models = {};

/*const namtaDataSchema = new Schema({
    sampleCode : String,
    sampleSiteName : String,
    samplingDate : String,
    waterTable : String,
    pH : String,
    ec : String,
    temperature : String,
    do : String,
    orp : String,
    alkalinity : String,
    ca : String,
    mg : String,
    k : String,
    na : String,
    fe : String,
    mn : String,
    hco3 : String,
    co3 : String,
    cl : String,
    no3 : String,
    so4 : String,
    siO2 : String,
    o18 : String,
    h2 : String,
    h3 : String,
    h3Err : String,
    c13Vpbd : String,
    c14Pmc : String,
    c14Year : String,
})*/
const namtaDataSchema = new Schema(namtaDataSchemaObj)

export const NamtaDataModel = model('namtadatas', namtaDataSchema)