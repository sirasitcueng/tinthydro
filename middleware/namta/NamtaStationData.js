import { model, models, Schema} from 'mongoose';
import { stationDataSchemaObj } from '../../context/SampleField';

models = {};

/*const namtaStationSchema = new Schema({
    sampleCode : String,
    sampleSiteName : String,
    lat : String,
    lng : String,
    developingDepth : String,
    screenDepth : String,
    altitude : String,
    aquiferName : String,
})*/

const namtaStationSchema = new Schema(stationDataSchemaObj);

export const NamtaStationModel = model('namtastations', namtaStationSchema)