import React, { useContext, useEffect} from "react";
import { ButtonRefContext } from '../context/ButtonRefContext'
import { StationContext } from '../context/StationContext'

import IotModal from "./ModalData/IotModal";
import RainModal from "./ModalData/RainModal";
import WellModal from "./ModalData/SampleModal/WellModal";
import NamtaModal from "./ModalData/SampleModal/NamtaModal";

import AddRainDataForm from "./AddDataForm/AddRainDataForm";
import Dashboard from "./DashboardData/Dashboard";
import AddLocationForm from "./AddDataForm/AddLocationForm";
import AddDataForm from "./AddDataForm/AddDataForm";
import AddImageForm from "./AddDataForm/AddImageForm";
//import { iotExample, namtaExample, rainExample, wellExample } from "../context/Example";
import useSWR from 'swr';




const GooMap = (props) => {
    const { displayState, STATE } = useContext(ButtonRefContext);
    const { h, serverName, isLogin } = props;
    const { map, setIotStationData, setWellStationData, setNamtaStationData, setRainStationData, setServerName } = useContext(StationContext)

    const { data : wellStationData } = useSWR('/api/wellStation', fetcher)
    const { data : namtaStationData  } = useSWR('/api/namtaStation', fetcher)
    const { data : iotStation } = useSWR('/api/iotStation', iotFetcher)
    const { data : rainStation  } = useSWR('/api/rainStation', rainFetcher)

    const { data } = useSWR('/api/rainStation', arrayFetcher)

    useEffect(() => {
        if (wellStationData) setWellStationData(wellStationData)
    }, [wellStationData])

    useEffect(() => {
        if (namtaStationData) setNamtaStationData(namtaStationData)
    }, [namtaStationData])

    useEffect(() => {
        if (iotStation) setIotStationData(iotStation)
    }, [iotStation])

    useEffect(() => {
        if (rainStation) setRainStationData(rainStation)
    }, [rainStation])

    /*useEffect(() => {
        let b = [];
        if (data) data.forEach(d => {
            b = [...b, {
                name : d.name,
                code : d.code,
                lat : d.lat,
                lng : d.lng,
            }]
        })
        console.log(b)
    }, [data])*/


    useEffect(() => {
        setServerName(serverName);
        //setIotStationData(iotStations);
        //setWellStationData(wellStationDatas);
        //setNamtaStationData(namtaStationDatas);
        //setRainStationData(rainStationDatas);

        //setIotStationData(iotExample);
        //setWellStationData(wellExample);
        //setNamtaStationData(namtaExample);
        //setRainStationData(rainExample);
        
    }, []);

    useEffect(() => {
        map.current = new google.maps.Map(document.getElementById("map"), {
            center: { lat: 	13.736717, lng: 100.523186 },
            zoom: 6.0,
        });
    }, []);

    return (
        <div className="map-row">
            <IotModal />
            <WellModal isLogin={isLogin} serverName={serverName}/>
            <NamtaModal isLogin={isLogin} serverName={serverName} />
            <RainModal />
            <div className="row">
                <div className="col-12 col-xl" style={{alignItems : 'center'}} hidden={displayState === STATE.STATE_DASHBOARD}>
                    <div id='map' style={{width: '100%', height: h || 800}}/>
                </div>
                <div className="col-12 col-xl-6 add-part" hidden={displayState !== STATE.STATE_RAIN}>
                    <AddRainDataForm />
                </div>
                <div className="col-12 col-xl-6 add-part" hidden={displayState !== STATE.STATE_SAMPLE}>
                    <AddDataForm />
                </div>
                <div className="col-12 col-xl-6 add-part" hidden={displayState !== STATE.STATE_SAMPLELOCATION}>
                    <AddLocationForm />
                </div>
                <div className="col-12 col-xl-6 add-part" hidden={displayState !== STATE.STATE_IMAGE}>
                    <AddImageForm />
                </div>
            </div>
            <div hidden={displayState !== STATE.STATE_DASHBOARD}>
                <Dashboard h={h} />
            </div>
        </div>
    )
}

const fetcher = (...args) => fetch(...args).then(async res => {
    let data = {};
    const jsonData = await res.json();
    if (jsonData) jsonData.forEach(d => {
        data[d._id] = d;
    });
    return data;
})

const iotFetcher = (...args) => fetch(...args).then(async res => {
    let data = {};
    const jsonData = await res.json();
    if (jsonData) jsonData.forEach(d => {
        data[d.IMEI] = d;
    });
    return data;
})

const rainFetcher = (...args) => fetch(...args).then(async res => {
    const wordToCamel = (wd) => {
        const result = wd.replace(/ /g, '')
        return result.charAt(0).toLowerCase() + result.slice(1);
    }
    let data = {};
    const jsonData = await res.json();
    if (jsonData) jsonData.forEach(d => {
        data[wordToCamel(d.name)] = {...d, keyName : wordToCamel(d.name)};
    });
    return data;
})

const arrayFetcher = (...args) => fetch(...args).then(res => res.json())


export default GooMap;