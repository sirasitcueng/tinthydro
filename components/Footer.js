import React, { useEffect, useState } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebookSquare, faInstagram, faTwitterSquare, faYoutube } from '@fortawesome/free-brands-svg-icons';


const Footer = props => {

    //const { visitors } = props;

    const [visitors, setVisitors] = useState(0);

    const getVisitorData = async () => {
        const rawData = await fetch(`/api/visitors`, {
            method: 'POST',
        })
        const jsonData = await rawData.json();
        if (Array.isArray(jsonData)) {
            return jsonData[0]['totalVisit']
        }
        else if (typeof jsonData === 'object') {
            return jsonData['totalVisit']
        }
        else {
            return 0;
        }
    }

    useEffect(() => {
        getVisitorData().then(res => setVisitors(res))
    }, []);

    return (
        <div id="footer">
            <div className='row footer-1'> 
                <div className="col-12">
                    {/*<a href="https://www.tint.or.th/main/index.php/th/" target="_blank">สทน</a>
                    <FontAwesomeIcon icon={faInstagram} />
                    <FontAwesomeIcon icon={faTwitterSquare} />
                    <FontAwesomeIcon icon={faFacebookSquare} />
                    <FontAwesomeIcon icon={faYoutube} />
                    <a href="https://www.facebook.com/thai.nuclear/" target="_blank">
                        <i className="fa fa-facebook-square fa-2x"></i>
                    </a>
                    <a href="https://www.instagram.com/tint.nuclear/" target="_blank">
                        <i className="fa fa-instagram fa-2x" aria-hidden="true"></i>
                    </a>
                    <a href="https://twitter.com/tint_nuclear" target="_blank">
                        <i className="fa fa-twitter-square fa-2x" aria-hidden="true"></i>
                    </a>
                    <a href="https://www.youtube.com/channel/UCh433WFwJT2_KpUHGqvRAYw" target="_blank">
                        <i className="fa fa-youtube-play fa-2x" aria-hidden="true"></i>
                    </a>
                    */}
                    <div className="row footer-img-container align-items-center">
                        <div className="col-auto col-sm-auto">
                            <img src="/Tint-logo-05.png"/>
                        </div>
                        <div className="col-auto col-sm-auto">
                            <img src="/Tint-logo-02.png"/>
                        </div>
                        <div className="col text-end">
                            จัดทำโดย: ห้องปฏิบัติการไอโซโทปไฮโดรโลยี <br />
                            ฝ่ายเทคโนโลยีสิ่งแวดล้อม ศูนย์วิจัยและพัฒนาเทคโนโลยีนิวเคลียร์ <br />
                            สถาบันเทคโนโลยีนิวเคลียร์แห่งชาติ(องค์การมหาชน) <br />
                            ติดต่อสอบถามข้อมูล โทร 0 2401 9885 หรือ 0 2401 9889 ต่อ 1302<br /><br />
                            ผู้เข้าชมเว็บ: {visitors}
                            
                        </div>

                    </div>
                    
                </div>
            </div>

            <div className="row">
                <div className="footer-2">
                    Copyright 2022. All Right Reserved.
                </div>
            </div>
        </div>
    )
}



export default Footer;