import React, { useContext, useEffect } from "react";
import { ButtonRefContext } from "../../context/ButtonRefContext";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearchLocation } from '@fortawesome/free-solid-svg-icons';
import { LangContext } from "../../context/LangContext";
import { TXT } from "../../context/TextDisplay";
import { FilterContext } from "../../context/FilterContext";




const SubHeader = () => {
    const { btnWellFilter, searchBoxRef, btnIotFilter, btnRainFilter, btnNamtaFilter, displayState, STATE } = useContext(ButtonRefContext);
    const { searchTxt, searchDashboard, setSearchTxt, setSearchDashboard, isIotFilter, isWellFilter, isRainFilter, isNamtaFilter, setIotFilter, setWellFilter, setRainFilter, setNamtaFilter, } = useContext(FilterContext);
    var reloadEnable = true;
    var rotation = 0;

    function AddReloadAnimation ()  {
        reloadEnable = false;
        rotation -= 360;
        document.getElementById('reload-icon').style.transform = `rotate(${rotation}deg)`
    }

    const { lang } = useContext(LangContext);

    const tabClick = (e) =>{
        e.preventDefault
        const Btn = document.getElementById(`${e.target.id}`);
        const val = e.target.value;
        if (e.target.tabIndex == 0){
            Btn.classList.add("default-btn-active");
            e.target.tabIndex = -1;
        } 
        else {
            Btn.classList.remove("default-btn-active");
            e.target.tabIndex = 0;
        }
        const type = e.target.id.split('-')[0];
        switch (type) {
            case 'iot' :
                setIotFilter(!isIotFilter);
                break;
            case 'well' :
                setWellFilter(!isWellFilter);
                break;
            case 'namta' :
                setNamtaFilter(!isNamtaFilter);
                break;
            case 'rain' :
                setRainFilter(!isRainFilter);
                break;
        }
    }

    const onSearchChange = e => {
        if (displayState === STATE.STATE_DASHBOARD) {
            setSearchDashboard(e.target.value);
        }
        else {
            setSearchTxt(e.target.value);
        }
    }
    useEffect(() => {
        setSearchTxt('');
        setSearchDashboard('');
        searchBoxRef.current.value = '';
    }, [displayState]);

    return (
        <div className="fillter-bar">
            <div className="row justify-content-between">
                <div className="col-12 fillter-block">
                    <div className="row">
                        <div className="col-12 col-sm-6 col-lg-auto">
                            {/*<DropdownButton as={ButtonGroup} variant="warning" title="บ่อน้ำบาดาล">
                                    <Dropdown.Item as={Button} ref={btnIotFilter} >มีการบันทึกข้อมูล</Dropdown.Item>
                                    <Dropdown.Item ref={btnWellFilter}>ไม่มีการบันทึกข้อมูล</Dropdown.Item>
                            </DropdownButton>*/}
                            <button className="default-btn default-btn-blue" ref={btnIotFilter} id="iot-btn" tabIndex="0" onClick={tabClick}>
                                <img src="./ICON_IOT_ONLINE_T2.png"/>
                                <span>{TXT.IOT_TAB[lang]}</span>
                            </button>
                        </div>
                        <div className="col-12 col-sm-6 col-lg-auto">
                            <button className="default-btn default-btn-lightBlue" id="well-btn" ref={btnWellFilter} tabIndex="0" onClick={tabClick}>
                                <img src="./ICON_WELL_T2.png"/>
                                <span>{TXT.WELL_TAB[lang]}</span>
                            </button>
                        </div>
                        <div className="col-12 col-sm-6 col-lg-auto">
                            <button className="default-btn default-btn-green" ref={btnRainFilter} id='rain-btn' tabIndex="0" onClick={tabClick}>
                                <img src="./ICON_RAIN_T2.png"/>
                                <span>{TXT.RAIN_TAB[lang]}</span>
                            </button>
                        </div>
                        <div className="col-12 col-sm-6 col-lg-auto">
                            <button className="default-btn default-btn-red" id='namta-btn' ref={btnNamtaFilter} tabIndex="0" onClick={tabClick}>
                                <img src="./ICON_NAMTA_T2.png"/>
                                <span>{TXT.NAMTA_TAB[lang]}</span>
                            </button>
                        </div> 
                        
                        <div className="col-12 col-lg-12 mt-3">
                            <div className="row search-container">
                                <div className="col-auto search-icon">
                                    <label htmlFor="search-form"><i className="fa fa-search fa-lg" aria-hidden="true"></i></label>
                                </div>
                                <div className="col col-lg-3 pd-0">
                                    <input type="text" className="form-control" id="search-form" onChange={onSearchChange} ref={searchBoxRef} />
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    )

}

export default SubHeader;

/* สำรองโค้ดปุ่มแบบเก่า
<div className="col-1">
                                    <button className="btn" ref={btnRefreshRef} onClick={AddReloadAnimation}>
                                        <svg id="reload-icon" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" >
                                            <path d="M13.5 2c-5.288 0-9.649 3.914-10.377 9h-3.123l4 5.917 4-5.917h-2.847c.711-3.972 4.174-7 8.347-7 4.687 0 8.5 3.813 8.5 8.5s-3.813 8.5-8.5 8.5c-3.015 0-5.662-1.583-7.171-3.957l-1.2 1.775c1.916 2.536 4.948 4.182 8.371 4.182 5.797 0 10.5-4.702 10.5-10.5s-4.703-10.5-10.5-10.5z"/>
                                        </svg>
                                    </button>
                                </div>

<div className="col-12 col-md-6 fillter-block">
                    <div className="row text-end">
                        <div className="col-12">
                            <button className="btn-AddData" ref={btnAddMarkerRef} hidden={!isLogin} >
                                <img src='./locationMarker.png'/>
                                <span>{`${lang == 'th'?`เพิ่มตำแหน่ง`:`Add Marker`}`}</span>
                            </button>
                        </div>
                        <div className="col-12">
                            <button className="btn-AddData " ref={btnAddRainRef}  hidden={!isLogin} >
                                <img src='./rainMarker.png'/>
                                <span>{`${lang == 'th'?`เพิ่มข้อมูลน้ำ`:`Add Rain Data`}`}</span>
                            </button>
                        </div>
                    </div>
                </div>
*/