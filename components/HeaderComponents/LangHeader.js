import { useContext } from "react";
import { LangContext } from "../../context/LangContext";


const LangHeader = () => {

    const { setLang } = useContext(LangContext);

    return (
        <div className="language-container">
            <div id="th" className="language-btn" onClick={() => setLang('th')}><span>T</span><span>H</span></div>
            <div id="en" className="language-btn" onClick={() => setLang('en')}><span>E</span><span>N</span></div>
        </div>
    )
}

export default LangHeader;