import React, { useContext } from "react";
import { auth, signOut } from '../../config/firebase';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { ButtonRefContext } from '../../context/ButtonRefContext';
import { LangContext } from "../../context/LangContext";
import { TXT } from "../../context/TextDisplay";


const Header = (props) => {
    const router = useRouter();
    const { lang } = useContext(LangContext);
    const { STATE, displayState, setDisplayState} = useContext(ButtonRefContext);

    const onSignOutClick = ev => {
        ev.preventDefault();
        signOut(auth)
            .then(() => alert(TXT.SIGN_OUT[lang] + TXT.SUCCESS[lang]))
            .then(() => setDisplayState(STATE.STATE_NORMAL))
            .then(() => router.push('/'))
            .catch(e => alert(e.message))
    }

    const onBtnClick = e => {
        const btnState = parseInt( e.target.value);
        if (displayState !== STATE.STATE_NORMAL) {
            setDisplayState(parseInt(STATE.STATE_NORMAL) )
        }
        else {
            setDisplayState(btnState)
        }
    }
    const mobileMenu = e =>{
        const addMenu = document.getElementById("add-menu");
        if (e.target.tabIndex =="1") {
            addMenu.classList.add("flex-right-mobile");
            e.target.tabIndex = "2";
        }
        else{
            addMenu.classList.remove("flex-right-mobile");
            e.target.tabIndex = "1";
        }
    }
    return (
        <div className="header-container">
            <div className="headTab-container">
                <div className="flex-left">
                    <div className="flex-item">
                        <Link href="/"><img src="/HYDRO_TINT_LOGO_TYPE01.png"/></Link>
                        <Link href='/'><span id="test" className="header-text desktop">{TXT.HEADER_TITLE[lang]}</span></Link>
                        <Link href='/'><span className="header-text mobile">{TXT.HEADER_TITLE_MOBILE[lang]}</span></Link>
                    </div>
                </div>
                <i id="menu-icon" tabIndex="1" className="fa fa-bars" onClick={mobileMenu}></i>
                <div id="add-menu" className="flex-right">
                    <div className="flex-item">
                        <button className="header-btn" onClick={onBtnClick} value={STATE.STATE_SAMPLELOCATION}>{TXT.ADD_LOCATION[lang]}</button>
                    </div>
                    <div className="flex-item">
                        <button className="header-btn" onClick={onBtnClick} value={STATE.STATE_RAIN}>{TXT.ADD_RAIN[lang]}</button>
                    </div>
                    <div className="flex-item">
                        <button className="header-btn" onClick={onBtnClick} value={STATE.STATE_SAMPLE}>{TXT.ADD_SAMPLE[lang]}</button>
                    </div>
                    <div className="flex-item">
                        <button className="header-btn" onClick={onBtnClick} value={STATE.STATE_IMAGE} >{TXT.ADD_IMAGE[lang]}</button>
                    </div>
                    <div className="flex-item">
                        <button className="header-btn" onClick={onBtnClick} value={STATE.STATE_DASHBOARD} >{TXT.DASHBOARD[lang]}</button>
                    </div>
                    <div className="flex-item">
                        <button className="red-btn" type="button" onClick={onSignOutClick}>{TXT.SIGN_OUT[lang]}</button>
                    </div>
                </div>
            </div>
        </div>
    )

    
    //โค้ดเก่าของอื๋อ เก็บไว้เป็นตัวอย่างเฉยๆ
    /*
    return (
        <div className="header-container">
            <div className="headTab-container"
            <Row>
                <Col md='auto'><Button variant="primary" onClick={onBtnClick} value={STATE.STATE_RAINSTATION} /*ref={btnAddRainStationRef}>add rain station</Button></Col>
                <Col md='auto'><Button variant="primary" onClick={onBtnClick} value={STATE.STATE_RAIN} /*ref={btnAddRainRef}>add rain data</Button></Col>
                <Col md='auto'><Button variant="primary" onClick={onBtnClick} value={STATE.STATE_WELL} /*ref={btnAddMarkerRef}>add nam badan</Button></Col>
                <Col md='auto'><Button variant="primary" onClick={onBtnClick} value={STATE.STATE_SAMPLE} /*ref={btnAddNamtaRef}>add nam ta</Button></Col>
                <Col md='auto'><Button variant="primary" onClick={onBtnClick} value={STATE.STATE_DASHBOARD} /*ref={btnAddNamtaRef}>dashboard</Button></Col>
                <Col bsPrefix="col me-auto" />
                <Col><Button onClick={onSignOutClick}>sign out</Button></Col>
            </Row>
        </div>
    )*/


}

export default Header;