import React, { useContext, useEffect, useRef, useState } from "react";
import Link from 'next/link'
import { auth } from "../../config/firebase";
import PasswordInput from '../DeleteData/PasswordInput'
import { signInWithEmailAndPassword } from "firebase/auth";
import { Form } from 'react-bootstrap'
import { LangContext } from "../../context/LangContext";
import { TXT } from "../../context/TextDisplay";


const HeaderPublic = () => {
    const [userEmail, setUserEmail] = useState('');
    const [userPassword, setUserPassword] = useState('');
    const [DisplaySignin, setDisplaySignin]= useState('hidden-signIn');

    const { lang } = useContext(LangContext);

    useEffect(() => {
        return () => {
            setUserEmail('');
            setUserPassword('');
        }
    }, []);
    
    const FormSignin = () => {
        const onSignInClick = e => {
            e.preventDefault();
            !userEmail ? alert(TXT.PLEASE_FILL[lang] + TXT.EMAIL[lang])
                : !userPassword ? alert(TXT.PLEASE_FILL[lang] + TXT.PASSWORD[lang])
                : signInWithEmailAndPassword(auth, userEmail, userPassword)
                .then(()=> setDisplaySignin('hidden-signIn'))
                .catch(e => alert(e.message))
        }

        return (
            <div className="signForm-container" >
                <Form.Group className="mb-3" controlId="formEmail">
                    <Form.Label>{TXT.EMAIL[lang]}</Form.Label>
                    <Form.Control type="email" name="email" autoComplete="off" 
                        onChange={e => setUserEmail(e.target.value)}/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formPassword">
                    <Form.Label>{TXT.PASSWORD[lang]}</Form.Label>
                    {/*<Form.Control type="password" name="password" autoComplete="off" 
                        onChange={e => setUserPassword(e.target.value)}/>*/}
                    <PasswordInput val={userPassword} setVal={setUserPassword} />
                </Form.Group>
                <div className="d-flex sign-btn-container">
                    <button className="close-btn" onClick={onSignInClick} >{TXT.SIGN_IN[lang]}</button>
                    <button className="close-btn red-btn" onClick={SigninPage}>{TXT.CLOSE[lang]}</button>
                </div>    
            </div>
        );
    }

    const SigninPage = () => {
        if (DisplaySignin === 'hidden-signIn'){
            setDisplaySignin('display-signIn');
        }else {
            setDisplaySignin('hidden-signIn');
        }
    }

    const rightItem = useRef(null);
    useEffect(() => {
        setTimeout(() => {
            if (rightItem.current) rightItem.current.hidden = false;
        }, 500);
    }, []);


    return (
        <div className="header-container" >
            <div className="headTab-container" >
                <div className="flex-left">
                    <div className="flex-item">
                        <Link href="/"><img src="/HYDRO_TINT_LOGO_TYPE01.png"/></Link>
                        <Link href='/'><span className="header-text">{TXT.HEADER_TITLE[lang]}</span></Link>
                    </div>
                </div>
                <div className="flex-right" ref={rightItem} hidden >
                    <div className="flex-item login-container" >
                        <button className="header-btn" onClick={SigninPage}>{TXT.LOG_IN[lang]}</button>
                    </div>
                </div>
            </div>
            <div className={DisplaySignin}>{FormSignin()}</div>
        </div>
    )
}

export default HeaderPublic;