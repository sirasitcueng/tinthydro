import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

import "react-datepicker/dist/react-datepicker.css";
//import SSRProvider from 'react-bootstrap/SSRProvider';
import Head from 'next/head';
import Footer from './Footer';
import { ModalProvider } from '../context/ModalContext';
import { ButtonRefProvider } from '../context/ButtonRefContext';
import { LangProvider } from '../context/LangContext';
import { StationProvider } from '../context/StationContext';
import { FilterProvider } from '../context/FilterContext';

//import Link from 'next/link';
//AIzaSyALGcoahSNIq2xsMADRkPpGF73c3tgQXFs

const Layout = (props) => {
    
    const { children } = props;

    const changeVisibility= () => {
        const t1 = document.getElementById('t1');
        if (t1.innerHTML === '&#xe8f5;') {
            t1.innerHTML = '&#xe8f4;';
        } else {
            t1.innerHTML = '&#xe8f5;';
        }
        return;
    }
    return (
        <div>
            <LangProvider><ModalProvider><StationProvider><FilterProvider><ButtonRefProvider>
            
                <Head>
                    <meta charSet="utf-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
                    <title>Thailand Database of Isotope Hydrology</title>
                    <link rel="stylesheet" href="/static/css/style.css" />
                    <link href="https://fonts.googleapis.com/css2?family=Material+Icons+Outlined"rel="stylesheet"/>
                    <link rel="icon" type="image/gif" href="HYDRO_TINT_LOGO_TYPE01.png" />
                    {/*<script 
                        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyALGcoahSNIq2xsMADRkPpGF73c3tgQXFs">AIzaSyBh9nFFG26UALBZDUAUQjI4rAiWgI7w3WI
                    </script>*/}
                    <script 
                        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBh9nFFG26UALBZDUAUQjI4rAiWgI7w3WI">
                    </script>
                    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"></link>
                    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                    <meta name="google-site-verification" content="9FEXJ6pl0HqKd2ZpjMUZoxxpTZiQnvArunaAH-8oGXw" />
                    <meta name="google-site-verification" content="gqlHeAUYEaNJYEizzyASlOHA5P5AfTvBbTEf0UX7sQ0" />
                </Head>

                    {children}
                <Footer/>

                
            </ButtonRefProvider></FilterProvider></StationProvider></ModalProvider></LangProvider>
        </div>
    )
}

export default Layout;