import { useContext, useEffect, useState } from "react"
import { ButtonRefContext } from "../../context/ButtonRefContext";
import { StationContext } from "../../context/StationContext";
import { LangContext } from "../../context/LangContext";
import { TXT } from "../../context/TextDisplay";
import DragDrop from "../ModalData/DragDrop";
import { SelectFormItem, TextFormItemGroup } from "./Common";

const initialState = {
    type : '',
    id : '',
    imgName : '',
    imgDesc : '',
    img : {
        data: null,
        contentType: '',
    }
}

const typeField = {
    iot : 'Groundwater Monitoring Well',
    well : 'Groundwater Sample',
    namta : 'River Water Sample',
    rain : 'Rain Station',
}

const imageField = {
    imgName : 'Image Name',
    imgDesc : 'Image Description',
}



const AddImageForm = () => {
    const [typingData, setTypingData] = useState(initialState);
    const [files, setFiles] = useState([]);
    const { displayState, setDisplayState } = useContext(ButtonRefContext);
    const { lang } = useContext(LangContext);
    const { iotStation, rainStationData, wellStationData, namtaStationData, serverName  } = useContext(StationContext);

    const iotOptions = Object.values(iotStation).reduce((obj, val) => ({...obj, [val.IMEI] : `${val.IMEI}: ${val.siteName}`}), {});
    const wellOptions = Object.values(wellStationData).reduce((obj, val) => ({...obj, [val._id] : `${val.sampleCode}: ${val.sampleSiteName}`}), {});
    const namtaOptions = Object.values(namtaStationData).reduce((obj, val) => ({...obj, [val._id] : `${val.sampleCode}: ${val.sampleSiteName}`}), {});
    const rainOptions = Object.values(rainStationData).reduce((obj, val) => ({...obj, [val.keyName] : `${val.code}: ${val.name}`}), {});

    useEffect(() => {
        return () => {
            setTypingData(initialState);
            setFiles([]);
        }
    }, []);

    useEffect(() => {
        setTypingData(initialState);
        setFiles([]);
    }, [displayState]);

    useEffect(async () => {
        if (files[0] && files[0].size > 1000000) {
            setFiles([]);
            alert('ไม่สามารถใส่ไฟล์ได้ เนื่องจากไฟล์มีขนาดเกิน 1 MB')
        }
        else if (files[0] && files[0].preview) {
            const res = await fetch(files[0].preview);
            const blob = await res.blob();
            const buffer = Buffer.from(await blob.arrayBuffer(), 'binary');
            setTypingData(d => ({...d, img : {
                data : buffer,
                contentType : files[0].type,
            }}));
        }
    }, [files])

    //useEffect(() => console.log(typingData.img), [typingData.img])

    const onAddClick = async () => {
        try {
            
            const { imgName, imgDesc, img, type, id } = typingData;
            if (!type || !img.data || !id) throw new Error('')
            let sampleSiteName, sampleCode;
            switch (type) {
                case 'iot' :
                    console.log(iotStation[id])
                    sampleSiteName = iotStation[id].siteName;
                    sampleCode = iotStation[id].IMEI;
                    break;
                case 'well' :
                    sampleSiteName = wellStationData[id].sampleSiteName;
                    sampleCode = wellStationData[id].sampleCode;
                    break;
                case 'namta' :
                    sampleSiteName = namtaStationData[id].sampleSiteName;
                    sampleCode = namtaStationData[id].sampleCode;
                    break;
                case 'rain' :
                    sampleSiteName = rainStationData[id].name;
                    sampleCode = rainStationData[id].code;
                    break;
            }
            const res = await fetch(`${serverName}api/images`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body : JSON.stringify({
                    imgName, imgDesc, img, type, sampleSiteName, sampleCode,
                })
            })  
            const raw = await res.json();
            alert(`เพิ่มข้อมูล ${raw.id} สำเร็จ`)
            setDisplayState(0);
        }
        catch (e) {
            alert(e);
        }
    }

    return (
        <div className="add-container">
            <h4 >Add a new image</h4>
            <br />
            <SelectFormItem title='Type' typingData={typingData} setTypingData={setTypingData} k='type' options={typeField} />
            <hr />
            <div hidden={typingData.type !== 'iot'}>
                <SelectFormItem title='Select Station/Site' typingData={typingData} setTypingData={setTypingData} k='id' options={iotOptions} />
            </div>
            <div hidden={typingData.type !== 'well'}>
                <SelectFormItem title='Select Station/Site' typingData={typingData} setTypingData={setTypingData} k='id' options={wellOptions} />
            </div>
            <div hidden={typingData.type !== 'namta'}>
                <SelectFormItem title='Select Station/Site' typingData={typingData} setTypingData={setTypingData} k='id' options={namtaOptions} />
            </div>
            <div hidden={typingData.type !== 'rain'}>
                <SelectFormItem title='Select Station/Site' typingData={typingData} setTypingData={setTypingData} k='id' options={rainOptions} />
            </div>
            <div hidden={!typingData.type}>
                <TextFormItemGroup typingData={typingData} setTypingData={setTypingData} field={imageField} />
                <DragDrop files={files} setFiles={setFiles}/>
            </div>
            
            <div className="add-btn-container">
                <button className="header-btn" onClick={onAddClick}>{TXT.ADD_DATA[lang]}</button>
                <button className="red-btn" onClick={() => setDisplayState(0)}>{TXT.CLOSE[lang]}</button>
            </div>
        </div>
    )
}

export default AddImageForm;