import { useContext, useEffect, useState } from "react";
import { Col, Form, Row, Tabs, Tab, Button } from "react-bootstrap";
import { ButtonRefContext } from "../../context/ButtonRefContext";
import { LangContext } from "../../context/LangContext";
import { initialTypingState, dataKeyPasteArray, sampleDataField } from "../../context/SampleField";
import { TXT } from "../../context/TextDisplay";
import RainDataForm from './RainDataForm';
import { SelectFormCustom, TextFormItem } from "./Common";
import { StationContext } from "../../context/StationContext";

const typeField = {
    well : 'Groundwater Sample',
    namta : 'River Water Sample',
}

const AddDataForm = () => {
    const [typingData, setTypingData] = useState(initialTypingState);
    const [type, setType] = useState('');
    const [ basicInfo, setBasicInfo ] = useState('')
    const { serverName, namtaStationData, wellStationData } = useContext(StationContext);
    const { displayState, setDisplayState } = useContext(ButtonRefContext);
    const {lang} = useContext(LangContext);
    const [optionArray, setOptionArray] = useState(['', '']);

    //const selectSampleRef = useRef(null);
    const initData = () => {
        setTypingData(initialTypingState);
        setType('');
        setBasicInfo('');
        setOptionArray(['', '']);
        //selectSampleRef.current.value = '';
    }

    useEffect(() => {
        return () => {
            initData();
        }
    }, [])

    useEffect(() => {
        initData();
    }, [displayState])

    const ItemGroup = (field) => {
        if (type) return Object.entries(sampleDataField[type][field]).map((k, i) => (
            <TextFormItem key={i} typingData={typingData} setTypingData={setTypingData} k={k[0]} title={k[1]} />
        ))
    }

    const onAddClick = async () => {
        await new Promise((resolve, reject) => {
            if (!basicInfo) reject('Invalid Data');
            const selectedData = type === 'namta' ? namtaStationData[basicInfo] : type === 'well' ? wellStationData[basicInfo] : {};
            if (!selectedData) reject('Invalid Data');
            const { sampleSiteName, sampleCode } = selectedData;
            if (!sampleSiteName && !sampleCode) reject('Invalid Data');
            resolve({...typingData, sampleSiteName, sampleCode})
        })
        .then(sendData => {
            return (type === 'namta') ? fetch(`${serverName}api/namtaData`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body : JSON.stringify(sendData)
            }) : fetch(`${serverName}api/wellData`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body : JSON.stringify(sendData)
            }) 
        })
        .then(async res => {
            const raw = await res.json();
            const { id } = raw;
            return id;
        })
        .then(id => {
            alert(`เพิ่มข้อมูล ${id} สำเร็จ`)
            setDisplayState(0);
        })
        .catch(e => console.log(e));
    }

    const onPasteClick = async event => {
        event.preventDefault();
        const multiLineText = await navigator.clipboard.readText();
        const lineArray = multiLineText.split('\n');
        const cellArray = lineArray[0].split('\t').slice(0, 35);
        cellArray.forEach((cell, index) => {
            setTypingData(d => {
                return {...d, [dataKeyPasteArray[index]] : cell}
            })
        })
    }

    const mappingOption = optionArray => {
        if (optionArray) return optionArray.map((v, i) => (
            <option key={i} value={v[0]}>{v[1]}</option>
        ))
    }
   
    
    useEffect(() => {
        switch (type) {
            case 'namta':
                const namtaArray = namtaStationData ? [['', ''], ...Object.values(namtaStationData).map(v => [v._id, `${v.sampleCode}: ${v.sampleSiteName}` ])] : ['']
                setOptionArray(namtaArray);
                break;
            case 'well' :
                const wellArray = wellStationData ? [['', ''], ...Object.values(wellStationData).map(v => [v._id, `${v.sampleCode}: ${v.sampleSiteName}` ])] : ['']
                setOptionArray(wellArray);
                break;
            default :
                break;
        }
        setBasicInfo('');
        //selectSampleRef.current.value = '';
    }, [type]);

    /*useEffect(() => {
        console.log(basicInfo)
    }, [basicInfo]);*/

    return (
        <div className="add-container">
            <SelectFormCustom title='Type' data={type} setData={setType} options={typeField} />
            <hr />
            <div hidden={type === 'rain'}>
                <Tabs defaultActiveKey="sample" id="tabsCustomModal" className="mb-3" >
                    <Tab eventKey='sample' title={`${lang=='en' ? "Sample Description":"ข้อมูลของตัวอย่าง"}`}>
                        <Form.Group as={Row} className="mb-3">
                            <Form.Label column className="col-auto col-md-3">Select Site</Form.Label>
                            <Col className="col-12 col-md col-lg-9">
                                <Form.Select type="text" /*ref={selectSampleRef}*/ value={basicInfo}
                                    onChange={e => setBasicInfo(e.target.value)}>
                                        {mappingOption(optionArray)}
                                </Form.Select>
                            </Col>
                        </Form.Group>
                        <TextFormItem typingData={typingData} setTypingData={setTypingData} k='samplingDate' title='Sampling Date'/>
                    </Tab>
                    <Tab eventKey='physical' title={`${lang=='en' ? "Physical Parameter":"ข้อมูลทางกายภาพ"}`}>
                        {ItemGroup('physical')}
                    </Tab>
                    <Tab eventKey='chemical' title={`${lang=='en' ? "Chemical Parameter":"ข้อมูลทางเคมี"}`}>
                        {ItemGroup('chemical')}
                    </Tab>
                    <Tab eventKey='isotope' title={`${lang=='en' ? "Isotopes":"ไอโซโทปรังสี"}`}>
                        {ItemGroup('isotope')}
                    </Tab>
                </Tabs>
                <div className="add-btn-container">
                    <button className="header-btn" onClick={onAddClick}>{TXT.ADD_DATA[lang]}</button>
                    <button className="header-btn" onClick={onPasteClick}>{TXT.PASTE[lang]}</button>
                    <button className="red-btn" onClick={() => setDisplayState(0)}>{TXT.CLOSE[lang]}</button>
                </div>
            </div>
            <div hidden={type !== 'rain'}>
                <RainDataForm /> 
            </div>
            
        </div>
    )
}

export default AddDataForm;