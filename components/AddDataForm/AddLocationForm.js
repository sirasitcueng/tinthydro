import { useContext, useEffect, useState } from 'react';
import { Form, Col, Row } from 'react-bootstrap';
import { ButtonRefContext } from '../../context/ButtonRefContext';
import { LangContext } from '../../context/LangContext';
import { sampleField, stationKeyPasteArray } from '../../context/SampleField';
import { StationContext } from '../../context/StationContext';
import { TXT } from '../../context/TextDisplay';
import { SelectFormItem, TextFormItem } from "./Common";

const initialState = {
    type: '',
    projectNumber : '',
    sampleCode : '',
    sampleSiteName : '',
    lat : '',
    lng : '',
    developingDepth : '',
    screenDepth : '',
    altitude : '',
    aquiferName : '',
    name : '',
    code : '',
}

const typeField = {
    well : 'Groundwater Sample',
    namta : 'River Water Sample',
    rain : 'New Rain Station'
}

const keyArray = [
    [...Object.keys(sampleField)],
    ['name', 'lat', 'lng', 'code']
]

const titleArray = [
    [...Object.values(sampleField)],
    ['Station Name', 'Latitude', 'Longitude', 'Station Code'] 
]

const AddLocationForm = () => {
    const [typingData, setTypingData] = useState(initialState);
    const { displayState, setDisplayState } = useContext(ButtonRefContext);
    const { lang } = useContext(LangContext);
    const { addNewStation, resetMarker, serverName } = useContext(StationContext);

    const ItemGroup = (index) => {
        return keyArray[index].map((k, i) => (
            <TextFormItem key={i} typingData={typingData} setTypingData={setTypingData} k={k} title={titleArray[index][i]} />
        ))
    }

    const onAddClick = async () => {
        const { type, projectNumber, sampleCode, sampleSiteName, lat, lng, developingDepth, screenDepth, altitude, aquiferName, name, code } = typingData;
        let fetchApi;
        let sendData = {};
        try {
            if (!(type && lat && lng)) throw new Error('Error: Invalid Position');
            switch (type) {
                case 'rain' :
                    if (!(name && code)) throw new Error('Error: Invalid Station Data')
                    fetchApi = `${serverName}api/rainStation`;
                    sendData = {name, lat, lng ,code}
                    break;
                case 'namta' :
                    if (!(sampleCode && sampleSiteName)) throw new Error('Error: Invalid Sample Data');
                    fetchApi = `${serverName}api/namtaStation`;
                    sendData = {projectNumber, sampleCode, sampleSiteName, lat, lng, developingDepth, screenDepth, altitude, aquiferName}
                    break;
                case 'well' :
                    if (!(sampleCode && sampleSiteName)) throw new Error('Error: Invalid Sample Data');
                    fetchApi = `${serverName}api/wellStation`;
                    sendData = {projectNumber, sampleCode, sampleSiteName, lat, lng, developingDepth, screenDepth, altitude, aquiferName};
                    break;
                default :
                    throw new Error('Error: Invalid Type')
            }
            const res = await fetch(fetchApi, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body : JSON.stringify(sendData)
            })  
            const raw = await res.json();
            const { id } = raw
            alert(`เพิ่มข้อมูล ${id} สำเร็จ`);
            resetMarker(type);
            type !== 'rain' ? addNewStation({
                type, id, data : {projectNumber, sampleCode, sampleSiteName, lat, lng, developingDepth, screenDepth, altitude, aquiferName, _id : id}
            }) : addNewStation({
                type, id, data : {name, lat, lng, code }
            })
            setDisplayState(0);
        }
        catch (e) {
            alert(e);
        }
        
    }

    const onPasteClick = async event => {
        event.preventDefault();
        const multiLineText = await navigator.clipboard.readText();
        const lineArray = multiLineText.split('\n');
        const cellArray = lineArray[0].split('\t').slice(0, 9);
        cellArray.forEach((cell, index) => {
            setTypingData(d => {
                return {...d, [stationKeyPasteArray[index]] : cell}
            })
        })
    }

    useEffect(() => {
        return () => {
            setTypingData(initialState);
        }
    }, []);

    useEffect(() => {
        setTypingData(initialState);
    }, [displayState]);

    return (
        <div className="add-container">
            <h4 >Add a new sample location or rain station</h4>
            <br />
            <SelectFormItem title='Type' typingData={typingData} setTypingData={setTypingData} k='type' options={typeField} />
            <hr />
            {typingData.type === 'rain' ? ItemGroup(1) : ItemGroup(0)}
            <div className="add-btn-container">
                <button className="header-btn" onClick={onAddClick}>{TXT.ADD_DATA[lang]}</button>
                <button className="header-btn" onClick={onPasteClick}>{TXT.PASTE[lang]}</button>
                <button className="red-btn" onClick={() => setDisplayState(0)}>{TXT.CLOSE[lang]}</button>
            </div>
        </div>
    )
}

export default AddLocationForm;