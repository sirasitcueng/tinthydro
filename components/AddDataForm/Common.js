import { Col, Form, Row } from "react-bootstrap";

export const TextFormItem = props => {
    const { typingData, setTypingData, k, title } = props;
    return title && k ? (
        <Form.Group as={Row} className="mb-3">
            <Form.Label column className="col-auto col-md-3">{title}</Form.Label>
            <Col className="col-12 col-md col-lg-9">
                <Form.Control value={typingData[k]} type="text" autoComplete="off" onChange={e => setTypingData({...typingData, [k]: e.target.value})}/>
            </Col>
        </Form.Group>
    ) : (<div />)
}

export const SelectFormItem = props => {
    const { title, typingData, setTypingData, k, options } = props;

    const MappingOptions = () => {
        if (options) return Object.entries(options).map((o, index) => (
            <option key={index} value={o[0]}>{o[1]}</option>
        ))
    }

    return (
        <Form.Group as={Row} className="mb-3">
            <Form.Label column className="col-auto col-md-3">{title}</Form.Label>
            <Col className="col-12 col-md col-lg-9">
                <Form.Select value={typingData[k]} type="text" autoComplete="off" 
                    onChange={e => setTypingData({...typingData, [k]: e.target.value})}>
                        <option value=''></option>
                        <MappingOptions />
                </Form.Select>
            </Col>
        </Form.Group>
    )
}

export const SelectFormCustom = props => {
    const { title, data, setData, options } = props;

    const MappingOptions = () => {
        if (options) return Object.entries(options).map((o, index) => (
            <option key={index} value={o[0]}>{o[1]}</option>
        ))
    }

    return (
        <Form.Group as={Row} className="mb-3">
            <Form.Label column className="col-auto col-md-3">{title}</Form.Label>
            <Col className="col-12 col-md col-lg-9">
                <Form.Select value={data} type="text" autoComplete="off" 
                    onChange={e => setData(e.target.value)}>
                        <option value=''></option>
                        <MappingOptions />
                </Form.Select>
            </Col>
        </Form.Group>
    )
}

export const TextFormItemGroup = (props) => {
    const { typingData, setTypingData, field } = props;
    return Object.entries(field).map((o, index) => (
        <TextFormItem key={index} title={o[1]} typingData={typingData} setTypingData={setTypingData} k={o[0]} />
    ))
}