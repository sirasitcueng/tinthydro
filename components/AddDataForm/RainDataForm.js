import { useState } from "react";
import { Col, Form } from "react-bootstrap";


const RainDataForm = () => {

    const [rainType, setRainType] = useState('');
    const [dateString, setDateString] = useState('');

    const mappingOption = optionArray => {
        if (optionArray) return optionArray.map((v, i) => (
            <option key={i} value={v}>{v}</option>
        ))
    }

    return (
        <div>
            <Form>
                <div className="row" style={{alignItems: 'center'}}>
                    <Col md="auto">Data Type</Col>
                    <Col>
                        <Form.Select value={rainType} onChange={e => setRainType(e.target.value)}>
                            {mappingOption(['', 'O18', 'H2', 'Precipitation'])}
                        </Form.Select>
                    </Col>
                    <Col md="auto"> Month-Year </Col>
                    <Col>
                        <input value={dateString} type="month" className="form-control" onChange={e => setDateString(e.target.value)}/>
                    </Col>
                    <Col>
                        <button type="button" className="header-btn" onClick={() => {}}>paste</button>
                    </Col>
                    <Col>
                        <button type="button" className="red-btn" onClick={() => {}}>clear</button>
                    </Col>
                </div>
            </Form>
        </div>
    )
}

export default RainDataForm;