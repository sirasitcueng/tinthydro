import { useEffect, useState, useContext } from "react"
import { Col, Form, Row, Table } from "react-bootstrap"
import { ButtonRefContext } from "../../context/ButtonRefContext";
import { LangContext } from "../../context/LangContext";
import { StationContext } from "../../context/StationContext";
import { TXT } from "../../context/TextDisplay";

const initialUpdateData = {
    type : '',
    dateString : '',
}

const initialDeleteData = {
    type : '',
    dateString : '',
    prov : 'all',
}

const AddRainDataForm = () => {

    const [updatingData, setUpdatingData] = useState({});
    const [otherUpdateData, setOtherUpdateData] = useState(initialUpdateData);

    const [deletingData, setDeletingData] = useState(initialDeleteData)
    const { rainStationData, serverName } = useContext(StationContext);
    const { displayState, setDisplayState } = useContext(ButtonRefContext);

    useEffect(() => {
        return () => initData();
    }, []);

    useEffect(() => {
        initData();
    }, [displayState])
    
    const initData = () => {
        setUpdatingData({});
        setOtherUpdateData(initialUpdateData);
        setDeletingData(initialDeleteData);
    }
    

    const mappingOption = optionArray => {
        if (optionArray) return optionArray.map((v, i) => (
            <option key={i} value={v}>{v}</option>
        ))
    }

    const mappingProvOption = () => {
        let nameArray = [['All', 'all']];
        for (const prov in rainStationData) {
            nameArray = [...nameArray, [rainStationData[prov].name, prov]];
        }
        return nameArray.map((v, i) => (
            <option key={i} value={v[1]}>{v[0]}</option>
        ))
    }

    const onPasteClick = ev => {
        ev.preventDefault();
        navigator.clipboard.readText().then(multilineText => {
            const lineArray = multilineText.split('\n');
            lineArray.pop();
            let rawData = {};
            lineArray.forEach(line => {
                let provData = {};
                const cellArray = line.split('\t')
                const dateArray = cellArray.slice(5)
                //console.log(cellArray[2])
                const rawProv = cellArray[2].replace(/ /g, '');
                const prov = rawProv.charAt(0).toLowerCase() + rawProv.slice(1);
                dateArray.forEach((d, i) => {
                    if (!isNaN(d)) provData = {...provData, [i + 1] : parseFloat(d)}
                })
                if (provData) rawData = {...rawData, [prov] : provData}
            })
            return rawData;
        })
        .then(dateObj => setUpdatingData(dateObj))
        .catch(e => console.log(e))
    }

    const onClearClick = () => {
        setUpdatingData(() => {});
    }

    const onUpdateClick = async () => {
        const { type, dateString} = otherUpdateData;
        try {
            if (!type) throw "โปรดใส่ประเภทค่า"
            if (!dateString) throw "โปรดใส่เดือนของข้อมูล"
            for (const prov in updatingData) {
                //console.log(prov, type, dateString)
                updateRainF(prov, type.toLowerCase(), dateString, updatingData[prov])
                    //.then(res => console.log(res))
                    //.then(() => alert('อัพเดทข้อมูลสำเร็จ'))
                    .catch(e => console.log(e));
            }
            alert('อัพเดทข้อมูลสำเร็จ')
            setDisplayState(0);
        }
        catch (e) {
            alert(e);
        }
        
    }

    const onDeleteClick = async () => {
        const { type, dateString, prov } = deletingData;
        try {
            if (!type) throw "โปรดใส่ประเภทค่า"
            if (!dateString) throw "โปรดใส่เดือนของข้อมูล"
            deleteRainF(prov, type.toLowerCase(), dateString)
                //.then(res => console.log(res))
                .then(() => alert('ลบข้อมูลสำเร็จ'))
                .then(() => setDisplayState(0))
                .catch(e => console.log(e));
            //alert('อัพเดทข้อมูลสำเร็จ')
        }
        catch (e) {
            alert(e);
        }
    }


    const updateRainF = async ( prov, type, dateString, dateObj ) => {
        const res = await fetch(`${serverName}/api/rainData`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body : JSON.stringify({
                prov, type, dateString, dateObj,
            })
        })
        const ret = await res.json()
        return ret;
    }

    const deleteRainF = async (prov, type, dateString) => {
        const res = await fetch(`${serverName}/api/rainData`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            },
            body : JSON.stringify({
                prov, type, dateString,
            })
        })
        const ret = await res.json()
        return ret;
    }

    const tableHeader = () => {
        let headerArray = ['Province']
        for (let i = 1; i <= 31; i++) {
            headerArray = [...headerArray, i]
        }
        return headerArray.map((v, i) => (
            <th key={i}>{v}</th>
        ))
    }

    const tableContent = () => {
        const provinceContent = (prov, dataObj) => {
            let dateArray = Array(32).fill('-');
            dateArray[0] = prov;
            for (const date in dataObj) {
                dateArray[date] = dataObj[date]
            }
            return dateArray.map((v, i) => (
                <td key={i}>{v || ''}</td>
            ))
        }
        const contentArray = updatingData ? Object.entries(updatingData) : [];
        if (contentArray) return contentArray.map((content, i) => (
            <tr key={i}>
                {provinceContent(content[0], content[1])}
            </tr>
        ))
    }

    const {lang} = useContext(LangContext);

    return (
        <div className="add-container date-form">
            <h4>Add Rain Water Data</h4>
            <div className="row first-form" style={{alignItems: 'center'}}>
                <Col className="col col-md-auto text"> Type</Col>
                <Col className="col-12 col-md col-lg">
                    <Form.Select value={otherUpdateData.type} onChange={e => setOtherUpdateData({...otherUpdateData, type : e.target.value})}>
                        {mappingOption(['', 'O18', 'H2', 'Precipitation'])}
                    </Form.Select>
                </Col>
                <Col className="col col-md-auto text"> Month-Year </Col>
                <Col className="col-12 col-md col-lg">
                    <input value={otherUpdateData.dateString} type="month" className="form-control" onChange={e => setOtherUpdateData({...otherUpdateData, dateString : e.target.value})}/>
                </Col>
                <Col className="col-6 col-md-auto">
                    <button type="button" className="header-btn" onClick={onPasteClick}>{TXT.PASTE[lang]}</button>
                </Col>
                <Col className="col-6 col-md-auto">
                    <button type="button" className="red-btn" onClick={onClearClick}>{TXT.CLEAR[lang]}</button>
                </Col>
            </div>
            <div style={{height: '300px', overflowY: 'auto'}}>
                <Table>
                    <thead><tr>
                        {tableHeader()}
                        
                    </tr></thead>
                    <tbody>
                        {tableContent()}
                    </tbody>
                </Table>

            </div>
            <div className="second-form"><button onClick={onUpdateClick} className="header-btn">{TXT.UPDATE_DATA[lang]}</button></div>
            <hr />


            <h4>Delete Rain Water Data</h4>
            <Form>
                <div className="row" style={{alignItems: 'center'}}>
                    <Col className="col col-md-auto text-second"> Type</Col>
                    <Col className="col-12 col-xl">
                        <Form.Select onChange={e => setDeletingData({...deletingData, type : e.target.value})}>
                            {mappingOption(['', 'O18', 'H2', 'Precipitation'])}
                        </Form.Select>
                    </Col>
                    <Col className="col col-md-auto text-second"> Month-Year </Col>
                    <Col className="col-12 col-xl">
                        <input type="month" className="form-control" onChange={e => setDeletingData({...deletingData, dateString : e.target.value})}/>
                    </Col>
                    <Col className="col col-md-auto text-second"> Station </Col>
                    <Col className="col-12 col-xl">
                        <Form.Select defaultValue="all" onChange={e => setDeletingData({...deletingData, prov : e.target.value})}>
                            {mappingProvOption()}
                        </Form.Select>
                    </Col>
                </div>
            </Form>
            <div className="second-form"><button onClick={onDeleteClick} className="red-btn">{TXT.DELETE_DATA[lang]}</button></div>
        </div>
    )
}

export default AddRainDataForm;