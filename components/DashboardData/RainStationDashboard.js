import { useContext } from "react";
import { Table } from "react-bootstrap";
import { StationContext } from "../../context/StationContext";
import { TableRow } from "./Common";

const headerArray = ['#', 'Station Name', 'Latitude', 'Longitude', 'Station Code']

const RainStationDashboard = () => {

    const { showedRainStation } = useContext(StationContext);

    const tableContent = () => {
        if (showedRainStation) {
            return showedRainStation.map((data, index) => (
                <tr key={index} >
                    <td>{index + 1}</td>
                    <TableRow cellArray={Object.values(data)} />
                </tr>
            ))
        }
    } 

    const tableHeader = () => {
        return headerArray.map((head, index) => (
            <th key={index}>{head}</th>
        ))
    }

    return (
        <div style={{overflowY: 'auto', overflowX: 'auto'}}>
            <Table>
                <thead><tr>
                    {tableHeader()}
                </tr></thead>
                <tbody>
                    {tableContent()}
                </tbody>
            </Table>
        </div>
    )
}

export default RainStationDashboard;