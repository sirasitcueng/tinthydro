import { Button, Table } from "react-bootstrap";
import { useContext, useState } from "react";
import { StationContext } from  "../../context/StationContext";
import DeleteModal from "../DeleteData/DeleteModal";
import { SampleTableHeader, TableRow } from "./Common";
import { checkPassword } from "../DeleteData/DeleteFunction";

const WellDashboard = () => {

    const { setWellStationData, wellStationData, showedWellSample, serverName, resetMarker } = useContext(StationContext);
    const [currentId, setCurrentId] = useState(null);
    const [password, setPassword] = useState('');
    const [showDeleteModal, setShowDeleteModal] = useState(false);

    const tableContent = () => {
        if (showedWellSample) {
            return showedWellSample.map((wellData, index) => (
                <tr key={index} >
                    <td>{index + 1}</td>
                    <TableRow cellArray={Object.values(wellData).slice(1)} />
                    <td><Button variant="danger" onClick={() => {setShowDeleteModal(true); setCurrentId(wellData._id)}}>Delete</Button></td>
                </tr>
            ))
        }
    } 

    const onDeleteClick = async () => {
        if (currentId && await checkPassword(password)) {
            await fetch(`${serverName}api/wellStation`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json'
                },
                body : JSON.stringify({
                    id : currentId,
                })
            })
            .then(() => {
                resetMarker('well');
                setWellStationData(Object.values(wellStationData).filter(m => m._id !== currentId).reduce( (obj, val) => ({...obj, [val._id] : wellStationData[val._id]}), {} ));
                alert(`ลบข้อมูล ${currentId} สำเร็จ`)
                setShowDeleteModal(false);
            })
            .catch(e => console.log(e))
        }
        else {
            console.log(password, await checkPassword(password))
        }
    }

    return (
        <div style={{overflowY: 'auto', overflowX: 'auto'}}>
            <DeleteModal val={password} setVal={setPassword} showDeleteModal={showDeleteModal} setShowDeleteModal={setShowDeleteModal}>
                <button className="red-btn" onClick={onDeleteClick} >Delete</button>
            </DeleteModal>
            <Table>
                <thead><tr>
                    <SampleTableHeader />
                </tr></thead>
                <tbody>
                    {tableContent()}
                </tbody>
            </Table>
        </div>
    )
}

export default WellDashboard;