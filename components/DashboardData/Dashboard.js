import { useContext, useEffect } from "react";
import { Tab, Tabs } from "react-bootstrap";
import { LangContext } from "../../context/LangContext";
import { TXT } from "../../context/TextDisplay";
import IotDashboard from "./IotDashboard";
import NamtaDashboard from "./NamtaDashboard";
import RainDashboard from "./RainDashboard";
import RainStationDashboard from "./RainStationDashboard";
import SampleDashboard from "./SampleDashboard";
import WellDashboard from "./WellDashboard";



const Dashboard = (props) => {

    const { h } = props;
    const { lang } = useContext(LangContext);
    const TXT_IOT = TXT.IOT_TAB[lang]
    const TXT_WELL = lang === 'en' ? 'Groundwater Sample List' : 'ภาพรวมน้ำบาดาล';
    const TXT_RAINSTATION = TXT.RAIN_TAB[lang];
    const TXT_RAIN = lang === 'en' ? 'Rainfall Data' : 'น้ำฝน';
    const TXT_NAMTA = lang === 'en' ? 'River Sample List' : 'ภาพรวมน้ำท่า';
    const TXT_WELLSAMPLE = lang === 'en' ? 'Groundwater Data' : 'ข้อมูลน้ำบาดาล';
    const TXT_NAMTASAMPLE = lang === 'en' ? 'River Data' : 'ข้อมูลน้ำท่า';

    return (
        <div className="container-fluid add-part" style={{textAlign: 'left', height: h, overflowY: 'auto'}}>
            <Tabs defaultActiveKey="iot" id="dashboardTabs" className="mb-3"> 
                <Tab eventKey='iot' title={TXT_IOT}>
                    <IotDashboard />
                </Tab>
                <Tab eventKey='well' title={TXT_WELL}>
                    <WellDashboard />
                </Tab>
                <Tab eventKey='wellSample' title={TXT_WELLSAMPLE}>
                    <SampleDashboard type='well' />
                </Tab>
                <Tab eventKey='namta' title={TXT_NAMTA}>
                    <NamtaDashboard />
                </Tab>
                <Tab eventKey='namtaSample' title={TXT_NAMTASAMPLE}>
                    <SampleDashboard type='namta' />
                </Tab>
                <Tab eventKey='rainStation' title={TXT_RAINSTATION}>
                    <RainStationDashboard />
                </Tab>
                <Tab eventKey='rainData' title={TXT_RAIN}>
                    <RainDashboard/>
                </Tab>
            </Tabs>
        </div>
    )

}

export default Dashboard;