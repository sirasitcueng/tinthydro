import { useContext } from "react";
import { Table } from "react-bootstrap";
import { StationContext } from "../../context/StationContext";
import { TableRow } from "./Common";

const headerArray = ['#', 'IMEI', 'Site Name', 'Latitude', 'Longitude', 'Status']

const IotDashboard = () => {

    const { showedIotStation } = useContext(StationContext);

    const tableContent = () => {
        if (showedIotStation) {
            return showedIotStation.map((data, index) => (
                <tr key={index} >
                    <td>{index + 1}</td>
                    <TableRow cellArray={Object.values(data)} />
                </tr>
            ))
        }
    } 

    const tableHeader = () => {
        return headerArray.map((head, index) => (
            <th key={index}>{head}</th>
        ))
    }

    return (
        <div style={{overflowY: 'auto', overflowX: 'auto'}}>
            <Table>
                <thead><tr>
                    {tableHeader()}
                </tr></thead>
                <tbody>
                    {tableContent()}
                </tbody>
            </Table>
        </div>
    )
}

export default IotDashboard;