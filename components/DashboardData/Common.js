import { sampleHeader } from "../../context/SampleField";




export const TableRow = props => {
    const { cellArray } = props;
    if (cellArray) return cellArray.map((v, i) => (
        <td key={i}>{v}</td>
    ))
}

export const SampleTableHeader = () => {
    return sampleHeader.map((head, index) => (
        <th key={index}>{head}</th>
    ))
}