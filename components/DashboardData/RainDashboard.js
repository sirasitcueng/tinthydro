import { useContext, useEffect, useState } from "react";
import { Button, Col, Form, Table } from "react-bootstrap";
import { StationContext } from "../../context/StationContext";



const RainDashboard = props => {

    const { rainStationData, serverName } = useContext(StationContext);

    const [selectedData, setSelectedData] = useState({
        dateString : '',
        prov : '',
    })

    const [tableData, setTableData] = useState({h2 : {}, o18 : {}, p : {}, dX : {}});

    useEffect(() => {
        return () => {
            setTableData({h2 : {}, o18 : {}, p : {}, dX : {}});
            setSelectedData({
                dateString : '',
                prov : '',
            })
        }
    }, [])

    const rowContent = (type, dataObj) => {
        let dateArray = Array(32).fill('-');
        dateArray[0] = type;
        for (const date in dataObj) {
            dateArray[date] = dataObj[date]
        }
        return dateArray.map((v, i) => (
            <td key={i}>{v || ''}</td>
        ))
    }

    /*const tableRow = (rowContent) => {
        if (rowContent) {
            return Object.values(rowContent).map((r, index) => (
                <td key={index}>{r}</td>
            ))
        }
    }*/

    const tableContent = () => {
        const { h2, o18, p, dX } = tableData;
        return (
            <tbody>
                <tr>
                    {rowContent('h2', h2)}
                </tr>
                <tr>
                    {rowContent('o18', o18)}
                </tr>
                <tr>
                    {rowContent('precipitation', p)}
                </tr>
                <tr>
                    {rowContent('D-excess', dX)}
                </tr>
            </tbody>
        )
        /*if (tableData) {
            return Object.values(tableData).map((type, index) => (
                <tr key={index} >{tableRow(wellData)}</tr>
            ))
        }*/
    }

    const tableHeader = () => {
        let headerArray = ['Type|Date']
        for (let i = 1; i <= 31; i++) {
            headerArray = [...headerArray, i]
        }
        return headerArray.map((v, i) => (
            <th key={i}>{v}</th>
        ))
    }

    const mappingOption = optionArray => {
        if (optionArray) return optionArray.map((v, i) => (
            <option key={i} value={v}>{v}</option>
        ))
    }

    const onQueryClick = () => {
        try {
            if (!selectedData.dateString) throw "โปรดใส่วันที่"
            if (!selectedData.prov) throw "โปรดใส่จังหวัด"
            getRainData(selectedData.prov, serverName).then(queryData => {
                const { h2, o18, precipitation } = queryData;
                if (h2 && h2[selectedData.dateString]) {
                    setTableData(v => {
                        return {...v, h2 : h2[selectedData.dateString]}
                    })
                }
                else {
                    setTableData(v => ({...v, h2 : {} }))
                }
                if (o18 && o18[selectedData.dateString]) {
                    setTableData(v => {
                        return {...v, o18 : o18[selectedData.dateString]}
                    })
                }
                else {
                    setTableData(v => ({...v, o18 : {} }))
                }
                if (precipitation && precipitation[selectedData.dateString]) {
                    setTableData(v => {
                        return {...v, p : precipitation[selectedData.dateString]}
                    })
                }
                else {
                    setTableData(v => ({...v, p : {} }))
                }
                if (h2 && h2[selectedData.dateString] && o18 && o18[selectedData.dateString]) {
                    let dxBuffer = {};
                    const hObj = h2[selectedData.dateString];
                    const oObj = o18[selectedData.dateString];
                    for (const date in hObj) {
                        if (oObj[date] && hObj[date]) {
                            dxBuffer[date] =  Math.round(  (parseFloat(hObj[date]) - 8 * parseFloat(oObj[date]) ) * 100  )/100
                        }
                    }
                    setTableData(v => {
                        return {...v, dX : dxBuffer}
                    })
                }
                else {
                    setTableData(v => ({...v, dX : {} }))
                }
            })
        }
        catch (e) {
            alert(e);
        }
    }


    return (
        <div>
            <div className="row" style={{alignItems: 'center'}}>
                <Col sm="auto"> Station </Col>
                <Col>
                    <Form.Select onChange={e => setSelectedData({...selectedData, prov : e.target.value})}>
                        {rainStationData ? mappingOption(['', ...Object.keys(rainStationData)]) : mappingOption([''])}
                    </Form.Select>
                </Col>
                <Col sm="auto"> Month-Year </Col>
                <Col>
                    <input type="month" className="form-control" onChange={e => setSelectedData({...selectedData, dateString : e.target.value})}/>
                </Col>
                <Col>
                    <Button variant="light" onClick={onQueryClick}>Search</Button>
                </Col>
            </div>
            <div style={{overflowY: 'auto', overflowX: 'auto', fontSize: '12px'}}>
                <Table>
                    <thead><tr>
                        {tableHeader()}
                    </tr></thead>
                    {tableContent()}

                </Table>
            </div>
        </div>
    )
}

const getRainData = async (prov, serverName) => {
    const rawData = await fetch(`${serverName}api/rainData`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body : JSON.stringify({
            prov : prov,
        })
    });
    const jsonData = await rawData.json();
    return jsonData;
}

export default RainDashboard;