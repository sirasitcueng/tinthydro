import { Button, Table } from "react-bootstrap";
import { useContext, useState } from "react";
import { StationContext } from  "../../context/StationContext";
import { ModalContext } from "../../context/ModalContext";
import { SampleTableHeader, TableRow } from "./Common";
import DeleteModal from "../DeleteData/DeleteModal";
import { checkPassword } from "../DeleteData/DeleteFunction";

const NamtaDashboard = () => {

    const { resetMarker, namtaStationData, setNamtaStationData, showedNamtaSample, serverName } = useContext(StationContext)
    //const { setShowDeleteModal } = useContext(ModalContext);
    const [currentId, setCurrentId] = useState(null);
    const [password, setPassword] = useState('');
    const [showDeleteModal, setShowDeleteModal] = useState(false);

    const tableContent = () => {
        if (showedNamtaSample) {
            return showedNamtaSample.map((namtaData, index) => (
                <tr key={index} >
                    <td>{index + 1}</td>
                    <TableRow cellArray={Object.values(namtaData).slice(1)} />
                    <td><Button variant="danger" onClick={() => {setShowDeleteModal(true); setCurrentId(namtaData._id)}}>Delete</Button></td>
                </tr>
            ))
        }
    } 

    const onDeleteClick = async () => {
        if (currentId && await checkPassword(password)) {
            await fetch(`${serverName}api/namtaStation`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json'
                },
                body : JSON.stringify({
                    id : currentId,
                })
            })
            .then(() => {
                resetMarker('namta');
                setNamtaStationData(Object.values(namtaStationData).filter(m => m._id !== currentId).reduce( (obj, val) => ({...obj, [val._id] : namtaStationData[val._id]}), {} ));
                alert(`ลบข้อมูล ${currentId} สำเร็จ`)
                setShowDeleteModal(false);
            })
            .catch(e => console.log(e))
        }
        else {
            console.log(await checkPassword(password))
        }
    };

    return (
        <div style={{overflowY: 'auto', overflowX: 'auto'}}>
            <DeleteModal val={password} setVal={setPassword} showDeleteModal={showDeleteModal} setShowDeleteModal={setShowDeleteModal}>
                <button className="red-btn" onClick={onDeleteClick}>Delete</button>
            </DeleteModal>
            <Table>
                <thead><tr>
                    <SampleTableHeader />
                </tr></thead>
                <tbody>
                    {tableContent()}
                </tbody>
            </Table>
        </div>
    )
}

export default NamtaDashboard;