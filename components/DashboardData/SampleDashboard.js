import { useContext, useEffect, useState } from "react";
import { Col, Form, Row, Tab, Tabs } from "react-bootstrap";
import { sampleDataField } from "../../context/SampleField";
import { StationContext } from "../../context/StationContext";


const SampleDashboard = props => {

    const { type } = props;
    const [optionArray, setOptionArray] = useState(['', '']);
    const [dataArray, setDataArray] = useState([]);
    const { wellStationData, namtaStationData, serverName } = useContext(StationContext);
    const locationData = type === 'namta' ? namtaStationData : type === 'well' ? wellStationData : {}; 


    useEffect(() => {
        if (locationData) setOptionArray([  ['', ''],
             ...Object.values(locationData).map(loc => [loc._id, `${loc.sampleCode}: ${loc.sampleSiteName}`])
        ]);
    }, [locationData]);

    const mappingOption = () => {
        return optionArray.map((o, i) => (
            <option key={i} value={o[0]}>{o[1]}</option>
        ))
    }

    const mappingData = (field) => {
        return dataArray.map((d, i) => (
            <EachData key={i} data={d} field={field}/>
        ))
    }

    const EachData = props => {
        const { data, field } = props;
        return (
            <div>
                <EachGroup field={field} data={data} />
            </div>
        )
    }

    const EachGroup = props => {
        const { data, field } = props;
        const GroupRow = () => Object.keys(sampleDataField[type][field]).map((k, i) => (
            <EachItem key={i} v={data[k]}/>
        ))
        const EachItem = props => <Col>{props.v}</Col>
        return (<div>
            <Row style={{textAlign: 'center'}}><Col md='auto' style={{fontWeight: 'bold', width: '120px'}}>{data.samplingDate}</Col><GroupRow /></Row><hr />
        </div>)
    }

    const TableHeader = props => {
        const { field } = props;
        const TableItem = () => Object.values(sampleDataField[type][field]).map((t, i) => (
            <Col key={i}>{t}</Col>
        ))
        return ( <div><Row style={{fontWeight: 'bold', textAlign: 'center'}}><Col md='auto' style={{width: '150px'}}>Sampling Date</Col><TableItem /></Row><hr /></div>)
    }

    const onSelectChange = async e => {
        const currentId = e.target.value;
        if (currentId) {
            try {
                const { sampleSiteName, sampleCode } = locationData[currentId];
                if (!sampleCode && !sampleSiteName) throw new Error('Invalid Data');
                const searchParams = new URLSearchParams({
                    sampleSiteName,
                    sampleCode,
                })
                const rawData = await fetch(`${serverName}api/${type}Data?${searchParams}`, {
                    method: 'GET',
                });
                const jsonData = await rawData.json();
                if (jsonData) setDataArray(jsonData);
            }
            catch (e) {
                console.log(e)
                
            }
        }
        else {
            setDataArray([]);
        }
        
        
    }

    return (
        <div>
            <Form.Group as={Row} className="mb-3">
                <Form.Label column md="auto">Sample</Form.Label>
                <Col md="auto">
                    <Form.Select type="text" 
                        onChange={onSelectChange}>
                            {mappingOption()}
                    </Form.Select>
                </Col>
            </Form.Group>
            <Tabs defaultActiveKey="physical" id="sampleDataTabs" className="mb-3">
                <Tab eventKey='physical' title="Physical Parameter">
                    <TableHeader field='physical' />
                    {mappingData('physical')}
                </Tab>
                <Tab eventKey='chemical' title="Chemical Parameter">
                    <TableHeader field='chemical'/>
                    {mappingData('chemical')}
                </Tab>
                <Tab eventKey='isotope' title="Isotopes">
                    <TableHeader field='isotope'/>
                    {mappingData('isotope')}
                </Tab>
            </Tabs>
            
        </div>
    )
}

export default SampleDashboard;