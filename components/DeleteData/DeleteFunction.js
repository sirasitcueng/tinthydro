import { EmailAuthProvider, reauthenticateWithCredential } from "firebase/auth"
import { auth } from "../../config/firebase"


export const checkPassword = async (password) => {
    const credential = EmailAuthProvider.credential(auth.currentUser.email, password);
    return await reauthenticateWithCredential(auth.currentUser, credential);
}