import React, { useContext } from "react";
import { Modal } from 'react-bootstrap';
import { TXT } from "../../context/TextDisplay";
import { LangContext } from "../../context/LangContext";
import PasswordInput from "./PasswordInput";

const DeleteModal = props => {

    const { lang } = useContext(LangContext)
    const { val, setVal, children, showDeleteModal, setShowDeleteModal } = props;
    

    const onHideModal = () => {
        setVal('');
        setShowDeleteModal(false);
    }

    return (
        <Modal className="add-part" show={showDeleteModal} size='lg' onHide={onHideModal}>
            <Modal.Body>
                {TXT.PLEASE_FILL[lang]}{TXT.PASSWORD[lang]}
                <PasswordInput val={val} setVal={setVal} />
            </Modal.Body>
            <Modal.Footer>
                <button className="header-btn" onClick={() => setShowDeleteModal(false)}>
                    Cancel
                </button>
                {children}
            </Modal.Footer>
        </Modal>
    )
}



export default DeleteModal;



