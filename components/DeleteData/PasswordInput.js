import { useRef } from "react";
import { Form } from "react-bootstrap"



const PasswordInput = props => {

    const textRef = useRef(null);
    const { val, setVal } = props;
    const {placeHolder, setPlaceHolder} = props;

    const onClick = () => textRef.current.removeAttribute('readOnly')

    return (
        <Form.Control 
            ref={textRef} 
            onClick={onClick}
            type='password'
            autoComplete="off"
            readOnly
            value={val}
            onChange={e => setVal(e.target.value)}
            placeholder={placeHolder}
        />
    )

}

export default PasswordInput;