import { useContext, useEffect, useRef, useState } from 'react';
import { Row, Col, Form } from 'react-bootstrap';
import { LangContext } from '../../../context/LangContext';
import { sampleDataField } from '../../../context/SampleField';
import { TXT } from '../../../context/TextDisplay';
import { checkPassword } from '../../DeleteData/DeleteFunction';
import PasswordInput from '../../DeleteData/PasswordInput';



const ChangeSampleData = props => {

    const { type, currentData, serverName, currentId } = props;
    const { lang } = useContext(LangContext);
    const [dataUpdated, setDataUpdated] = useState({ keyData: '', valueData: '' });
    const valueDataRef = useRef(null);
    const [password, setPassword] = useState('');

    const onUpdateClick = async () => {
        const { keyData, valueData } = dataUpdated;
        await fetch(`${serverName}api/${type === 'namta' ? 'namtaData' : type === 'well' ? 'wellData' : ''}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body : JSON.stringify({
                id : currentId,
                update : {[keyData] : valueData},
            })
        })
        .then(() => alert('อัพเดทสำเร็จ หากข้อมูลยังไม่เปลี่ยนแปลงโปรดรีเฟรช'))
        .catch(() => alert('พบข้อผิดพลาด'))
    }

    const onDeleteClick = async () => {
        if (currentId && await checkPassword(password)) {
            await fetch(`${serverName}api/${type === 'namta' ? 'namtaData' : type === 'well' ? 'wellData' : ''}`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json'
                },
                body : JSON.stringify({
                    id : currentId,
                })
            })
            .then(() => {
                alert(`ลบข้อมูล ${currentId} สำเร็จ`)
            })
            .catch(e => console.log(e))
        }
        else {
            console.log(namtaModalData)
        }
    }

    useEffect(() => {
        if (valueDataRef.current && currentData[dataUpdated.keyData]) {
            valueDataRef.current.value = currentData[dataUpdated.keyData];
            setDataUpdated({...dataUpdated, valueData: currentData[dataUpdated.keyData]})
        }
        else if (valueDataRef.current) {
            valueDataRef.current.value = '';
            setDataUpdated({...dataUpdated, valueData: ''})
        }
    }, [dataUpdated.keyData]);

    const mappingOption = () => {
        return Object.entries(sampleDataField[type].all).map((v, i) => (
            <option key={i} value={v[0]}>{v[1]}</option>
        ))
    }

    return (
        <div>
            <Row>
                <Col className="mb-3" lg='12'><div style={{fontWeight: 'bold'}} >Add or Change</div></Col>
                <Col>
                    <Form.Group as={Row} className="mb-3">
                        <Form.Label column lg="auto">Field</Form.Label>
                        <Col lg="auto">
                            <Form.Select value={dataUpdated.keyData} onChange={e => setDataUpdated({...dataUpdated, keyData: e.target.value})}>
                                {mappingOption()}
                            </Form.Select>
                        </Col>
                        <Form.Label column lg="auto">Value</Form.Label>
                        <Col>
                            <Form.Control ref={valueDataRef} value={dataUpdated.valueData} type="text" autoComplete="off"
                                onChange={e => setDataUpdated({...dataUpdated, valueData: e.target.value})}/>
                        </Col>
                        <Col><button className="modal-default-btn" onClick={onUpdateClick}>Update</button></Col>
                    </Form.Group>
                </Col>
            </Row>
            <hr />
            <Row>
                <Col className="mb-3" lg='12'><div style={{fontWeight: 'bold'}} >Delete</div></Col>

                <Col className="col-auto col-md"><PasswordInput val={password} setVal={setPassword} placeHolder={`${TXT.PLEASE_FILL[lang]}${TXT.PASSWORD[lang]}`} /></Col>
                <Col>
                    <button className="btn btn-danger mobile-mt-3" onClick={onDeleteClick}>Delete</button>
                </Col>
            </Row>
            <hr />
        </div>
    )
}

export default ChangeSampleData;