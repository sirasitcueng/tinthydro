import { useContext, useEffect, useState } from 'react';
import { Tabs, Tab, Row, Col } from 'react-bootstrap';
import { ModalContext } from '../../../context/ModalContext';
import { sampleDataField } from '../../../context/SampleField';
import ChangeSampleData from './ChangeSampleData';



const SampleData = props => {

    const { isLogin, currentId, serverName, type } = props;
    const [currentData, setCurrentData] = useState({});
    const { namtaModalData, wellModalData } = useContext(ModalContext)
    const { query } = type === 'namta' ? namtaModalData || {} : type === 'well' ? wellModalData || {} : {};

    useEffect(() => {
        if (currentId) {
            setCurrentData(query.find(q => q._id === currentId))
        }
        else {
            setCurrentData({});
        }
    }, [currentId]);

    useEffect(() => {
        return () => {
            setCurrentData({});
        }
    }, []);

    const EachItem = props => {
        const { k, t } = props;
        return (
            <div>
                <div><span style={{fontWeight: 'bold'}}>{t} :</span> {currentData[k] || '-'}</div> 
                <hr />
            </div>
        )
    }

    const ItemGroup = props => {
        const { field } = props;
        if (currentData) return Object.entries(sampleDataField[type][field]).map((k, i) => (
            <Col className="col-12 col-lg-6" key={i}>
                <EachItem k={k[0]} t={k[1]} />
            </Col>
        ))
    }

    return (
        <div  hidden={!currentId}>
            <Tabs defaultActiveKey="physical" id="tabsCustomModal" className="mb-3">
                <Tab eventKey='physical' title="Physical Parameter">
                    <Row>
                        <ItemGroup field='physical' />
                    </Row>
                </Tab>
                <Tab eventKey='chemical' title="Chemical Parameter">
                    <Row>
                        <ItemGroup field='chemical' />
                    </Row>
                </Tab>
                <Tab eventKey='isotope' title="Isotopes">
                    <Row>
                        <ItemGroup field='isotope' />
                    </Row>
                </Tab>
                <Tab eventKey='props' title="Properties" disabled={!currentId} hidden={!isLogin}>
                    <ChangeSampleData currentData={currentData} currentId={currentId} serverName={serverName} type={type} />
                </Tab>
            </Tabs>
        </div>
        
    )
}


export default SampleData;