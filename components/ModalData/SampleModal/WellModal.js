import React, { useContext, useEffect, useState } from "react";
import { Col, Form, Modal, Row } from 'react-bootstrap';

import { ModalContext } from "../../../context/ModalContext";
import { sampleField } from "../../../context/SampleField";
import SampleData from "./SampleData";
import ShowImage from "../ShowImage";
import ExternalMapLink from "../ExternalMapLink";

const WellModal = props => {

    const { showWellModal, setShowWellModal, wellModalData } = useContext(ModalContext);
    const { isLogin, serverName } = props;
    const { sampleSiteName, sampleCode, lat, lng } = wellModalData || {};
    const [currentId, setCurrentId] = useState('');
    const [dateArray, setDateArray] = useState(['']);

    useEffect(() => {
        return () => {
            setCurrentId('');
            setDateArray(['']);
        }
    }, []);

    useEffect(() => {
        const { query } = wellModalData || {};
        if (query) {
            setDateArray(['', ...query.map(d => [d._id, d.samplingDate])])
        }
    }, [wellModalData]);

    const EachItem = props => {
        const { k, t } = props
        return (
            <div>
                <div style={{fontWeight: 'bold'}}>{t}</div> 
                {wellModalData[k] || '-'}
                <hr />
            </div>
        )
    }

    const ItemGroup = () => {
        return Object.entries(sampleField).map((k, i) => (
            <Col className="col-12 col-lg-6" key={i}>
                <EachItem key={i} k={k[0]} t={k[1]} />
            </Col>
        ))
    }

    const mappingOption = optionArray => {
        if (optionArray) return optionArray.map((v, i) => (
            <option key={i} value={v[0]}>{v[1]}</option>
        ))
    }

    const onHideModal = () => {
        setShowWellModal(false);
        setCurrentId('');
        setDateArray(['']);
    }

    return (
        <Modal className="add-part" show={showWellModal} size="xl" onHide={onHideModal}>
            <Modal.Header closeButton>
                <Modal.Title>{sampleSiteName}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Row>
                    {ItemGroup()}
                    <Col className="col-12 col-lg-6">
                        <ExternalMapLink lat={lat} lng={lng} type='well'/>
                    </Col>
          
                    <Col className="mt-3">
                        <ShowImage serverName={serverName} type='well' sampleSiteName={sampleSiteName} sampleCode={sampleCode} />
                        <Form.Group as={Row} className="mb-3">
                            <Form.Label column md="auto"><span className="f-bold">Sampling Date</span></Form.Label>
                            <Col md="auto">
                                <Form.Select type="text" 
                                    onChange={e => setCurrentId(e.target.value)}>
                                        {mappingOption(dateArray)}
                                </Form.Select>
                            </Col>
                        </Form.Group>
                        <SampleData isLogin={isLogin} currentId={currentId} serverName={serverName} type='well'/>
                    </Col>
                </Row>

            </Modal.Body>
            <Modal.Footer>
                <button className="red-btn" onClick={() => setShowWellModal(false)}>
                    Close
                </button>
            </Modal.Footer>
        </Modal>
    )
}



export default WellModal;



