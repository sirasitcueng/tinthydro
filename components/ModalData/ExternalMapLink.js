import { Col, Row } from "react-bootstrap";


const ExternalMapLink = props => {
    const { lat, lng, type } = props;
    return type === 'namta' || type === 'well' ? (
        <div>
            <div style={{fontWeight: 'bold'}}>Google Map Link</div> 
            <a target="_blank" href={`https://www.google.com/maps/search/?api=1&query=${lat},${lng}`}>link</a>
            <hr />
        </div>
    ) : type === 'iot' || type === 'rain' ? (
        <div>
            <Row>
                <Col><div style={{fontWeight: 'bold'}}>Google Map Link</div></Col>
                <Col><a target="_blank" href={`https://www.google.com/maps/search/?api=1&query=${lat},${lng}`}>link</a></Col>
            </Row>
            <hr />
        </div>
    ) : <div />
}

export default ExternalMapLink;