import React, { useContext } from "react";
import { Button, Col, Modal, Row } from 'react-bootstrap';

import { ModalContext } from "../../context/ModalContext";
import ExternalMapLink from "./ExternalMapLink";

import RainScatterChart from "./RainScatterChart";
import ShowImage from "./ShowImage";


const RainModal = () => {

    const { showRainModal, setShowRainModal, rainModalData } = useContext(ModalContext);

    const { name, code, lat, lng, o18, h2, precipitation  } = rainModalData || {};

    const BasicItem = props => {
        return (
            <div>
                <Row>
                    <Col><div style={{fontWeight: 'bold'}}>{props.title}</div></Col>
                    <Col>{props.val}</Col>
                </Row>
                <hr />
            </div>
            
        )
    }

    return (
        <Modal show={showRainModal} size="xl" onHide={() => setShowRainModal(false)}>
            <Modal.Header closeButton>
                <Modal.Title>{rainModalData ? rainModalData.name : ''}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Row>
                    <Col lg="12">
                        <Row>
                            <Col className="col-12 col-lg-6">
                                <BasicItem title="Name" val={name} />
                                <BasicItem title="Code" val={code} />
                                <ExternalMapLink lat={lat} lng={lng} type='rain'/>
                            </Col>
                            <Col className="col-12 col-lg-6">
                                <BasicItem title="Latitude" val={lat} />
                                <BasicItem title="Longitude" val={lng} />
                            </Col>
                        </Row>
                        
                        
                        
                    </Col>
                    <Col className="add-part">
                        <ShowImage type='rain' sampleSiteName={name} sampleCode={code} />
                        <RainScatterChart o18={o18} h2={h2} precipitation={precipitation}/>
                        <div>ข้อมูลจาก สสน </div>
                    </Col>
                </Row>
                
            </Modal.Body>
            <Modal.Footer>
                <button className="red-btn" onClick={() => setShowRainModal(false)}>
                    Close
                </button>
            </Modal.Footer>
        </Modal>
    )
}

export default RainModal;