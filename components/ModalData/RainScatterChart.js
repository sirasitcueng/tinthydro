import { useEffect, useRef, useState } from "react";
import { Button, Col, Form, Tab, Tabs } from "react-bootstrap";
import DateForm from "./DateForm";

const RainScatterChart = (props) => {

    const o18ChartRef = useRef(null);
    const h2ChartRef = useRef(null);
    const dXChartRef = useRef(null);

    const [latestData, setLatestData] = useState({o18: [], h2: [], dX: []});
    const { o18, h2 , precipitation } = props;
    const [dateRange, setDateRange] = useState({from : null, to : null});

    const [key, setKey] = useState('o18');

    useEffect(() => {
        return () => {
            setKey('o18');
            setLatestData({o18: [], h2: [], dX: []});
            setDateRange({from : null, to : null});
        }
    }, []);

    const initChart = () => {
        const p = new Promise((resolve) => {
            let o18Buf = [];
            let h2Buf = [];
            let dXBuf = [];
            const { from, to } = dateRange;
            for (const monthYear in o18) {
                const [year, month] = monthYear.split('-').map(v => parseInt(v));
                for (const day in o18[monthYear]) {
                    if (o18[monthYear][day] !== null && precipitation[monthYear]) {
                        console.log(o18)
                        o18Buf = [...o18Buf, [new Date(year, month - 1, day), o18[monthYear][day], precipitation[monthYear][day]  ]]
                    }
                }
            }
            for (const monthYear in h2) {
                const [year, month] = monthYear.split('-').map(v => parseInt(v));
                for (const day in h2[monthYear]) {
                    if (h2[monthYear][day] !== null  && precipitation[monthYear]) {
                        h2Buf = [...h2Buf, [new Date(year, month - 1, day), h2[monthYear][day], precipitation[monthYear][day]  ]]
                    }
                }
            }
            for (const monthYear in o18) {
                const [year, month] = monthYear.split('-').map(v => parseInt(v));
                if (h2[monthYear]  && precipitation[monthYear]) for (const day in h2[monthYear]) {
                    const h2Val = h2[monthYear][day];
                    const o18Val = o18[monthYear][day]
                    if (h2Val && o18Val){
                        const dx = parseFloat(h2Val) - 8 * parseFloat(o18Val);
                        dXBuf = [...dXBuf, [new Date(year, month - 1, day), dx, precipitation[monthYear][day]  ]]
                    }
                }
            }

            if (from) {
                o18Buf = o18Buf.filter(data => data[0] >= from)
                h2Buf = h2Buf.filter(data => data[0] >= from)
                dXBuf = dXBuf.filter(data => data[0] >= from)
            }
            if (to) {
                o18Buf = o18Buf.filter(data => data[0] <= to)
                h2Buf = h2Buf.filter(data => data[0] <= to)
                dXBuf = dXBuf.filter(data => data[0] <= to)
            }
            resolve({o18: o18Buf, h2: h2Buf, dX: dXBuf});
        })
        p.then(dataBuf => {
            setLatestData(dataBuf);
        })
    }

    useEffect(() => {
        initChart();
    }, []);

    const options = (l, n) => {
        return {
            chartArea : {
                width: '75%',
            },
            height: 350,
            series: {
                0: {
                    type: "scatter",
                    targetAxisIndex: 0,
                    color: "blue",
                    pointSize: '5'
                },
                1: {
                    type: "bars",
                    targetAxisIndex: 1,
                    color: "red"
                },
            },
            vAxes: {
                // Adds titles to each axis.
                0: {
                    title: n + ' [‰ (V-SMOW)]',
                    titleTextStyle: {
                        italic: false,
                        bold: true,
                        color: 'blue'
                    }
                },
                1: {
                    title: 'Precipitation [mm]',
                    titleTextStyle: {
                        italic: false,
                        bold: true,
                        color: 'red'
                    }
                },
                
            },
            curveType: 'function',
            hAxis: {
                format: 'd/M/yy',
            },
        }
    }

    useEffect(() => {
        let addedData = [];
        let addedTitle = '';
        let addedRef;
        switch (key) {
            case 'o18' :
                addedData = latestData.o18;
                addedTitle = '¹⁸O';
                addedRef = o18ChartRef.current;
                break;
            case 'h2' :
                addedData = latestData.h2;
                addedTitle = '²H';
                addedRef = h2ChartRef.current;
                break;
            case 'dx' :
                addedData = latestData.dX;
                addedTitle = 'D-excess';
                addedRef = dXChartRef.current;           
                break;
            default :
                break;
        }

        const drawChart = () => {
            const data = new google.visualization.DataTable();
            data.addColumn('datetime', 'Time');
            data.addColumn('number', addedTitle);
            data.addColumn('number', 'Precipitation');
            data.addRows(addedData)
            const chart = new google.visualization.LineChart(addedRef);
            chart.draw(data, options(addedData.length, addedTitle));
        }
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);

    }, [latestData, key])

    
    const onSearchClick = () => {
        initChart();
    }


    return (
        <div>
            <DateForm dateRange={dateRange} setDateRange={setDateRange} onSearchClick={onSearchClick} />
            <hr />
            <Tabs id="tabsRainModal" className="mb-3" activeKey={key} onSelect={(k) => setKey(k)}>
                <Tab eventKey="o18" title="¹⁸O">
                    <div style={{overflowX: 'hidden', width: '100%', overflowY: 'hidden'}} >
                        <div ref={o18ChartRef} />
                    </div>
                </Tab>
                <Tab eventKey="h2" title="²H">
                    <div style={{overflowX: 'hidden', width: '100%', overflowY: 'hidden'}} >
                        <div ref={h2ChartRef} />
                    </div>
                </Tab>
                <Tab eventKey="dx" title="D-excess">
                    <div style={{overflowX: 'hidden', width: '100%', overflowY: 'hidden'}} >
                        <div ref={dXChartRef} />
                    </div>
                </Tab>
                
            </Tabs>
        </div>
        
    )

}


export default RainScatterChart;

