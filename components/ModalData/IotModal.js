import React, { useContext } from "react";
import { Col, Modal, Row } from 'react-bootstrap';
import { ModalContext } from "../../context/ModalContext";
import ExternalMapLink from "./ExternalMapLink";
import IotScatterChart from "./IotScatterChart";
import ShowImage from "./ShowImage"; 

const IotModal = () => {

    const { showIotModal, setShowIotModal, iotModalData } = useContext(ModalContext);
    const { siteName, IMEI, lat, lng, isOnline } = iotModalData || {};

    return (
        <Modal className="add-part" show={showIotModal} size="xl" onHide={() => setShowIotModal(false)}>
            <Modal.Header closeButton>
                <Modal.Title>{siteName || ''}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Row>
                    <Col lg='12'>
                        <Row>
                            <Col className="col-12 col-lg-6">
                                <ItemList k="Site Name" v={siteName} />
                                <ItemList k="IMEI" v={IMEI} />
                                <ItemList k="Status" v={isOnline ? 'Online' : 'Offline'} />
                            </Col>
                            <Col className="col-12 col-lg-6">
                                <ItemList k="Latitude" v={lat} />
                                <ItemList k="Longitude" v={lng} />
                                <ExternalMapLink lat={lat} lng={lng} type='iot'/>
                            </Col>
                        </Row>
                    </Col>
                    <Col className="mt-4">
                        <ShowImage type='iot' sampleSiteName={siteName} sampleCode={IMEI} />
                        <IotScatterChart />
                    </Col>
                    
                </Row>
                
            </Modal.Body>
            <Modal.Footer>
                <button className="red-btn" onClick={() => setShowIotModal(false)}>
                    Close
                </button>
            </Modal.Footer>
        </Modal>
    )
}

const ItemList = props => {
    const { k, v } = props || {};
    return (
        <div>
            <Row>
                <Col><div style={{fontWeight: 'bold'}}>{k}</div></Col>
                <Col>{v}</Col>
            </Row>
            <hr />
        </div>
    )
}

export default IotModal;