import { useContext, useEffect, useRef, useState } from "react";
import { Tab, Tabs } from "react-bootstrap";
import { ModalContext } from "../../context/ModalContext";
import DateForm from "./DateForm";

const IotScatterChart = () => {

    const depthChartRef = useRef(null);
    const ecChartRef = useRef(null);
    const tempChartRef = useRef(null);

    const [latestData, setLatestData] = useState({depth: [], ec: [], temp: []});
    const [key, setKey] = useState('depth');
    const [dateRange, setDateRange] = useState({from : null, to : null});

    const { iotModalData } = useContext(ModalContext);
    const { queryData } = iotModalData || {};

    useEffect(() => {
        return () => {
            setDateRange({from : null, to : null})
            setLatestData({depth: [], ec: [], temp: []});
            setKey('depth');
        }
    }, []);

    useEffect(() => {

        const p = new Promise((resolve) => {
            let depthBuf = [];
            let ecBuf = [];
            let tempBuf = [];
            if (queryData) queryData.forEach(data => {
                const { date, depth, ec, temp } = data || {};
                depthBuf = [...depthBuf, [date, depth]]
                ecBuf = [...ecBuf, [date, ec]]
                tempBuf = [...tempBuf, [date, temp]]
            })
            resolve({depth: depthBuf, ec: ecBuf, temp: tempBuf})
        })
        p.then(dataBuf => {
            setLatestData(dataBuf);
            
        })
        
    }, []);



    useEffect(() => {
        drawOneChart(latestData, key);
    }, [latestData, key])

    const drawOneChart = (mLatestData, mKey) => {
        let addedData = [];
        let addedTitle = '';
        let addedRef;
        let addedOption = {};
        switch (mKey) {
            case 'depth' :
                addedData = mLatestData.depth;
                addedTitle = 'Groundwater level (m bgl)';
                addedRef = depthChartRef.current;
                addedOption = depthOption;
                break;
            case 'ec' :
                addedData = mLatestData.ec;
                addedTitle = 'Conductivity (µS/cm)';
                addedRef = ecChartRef.current;
                addedOption = options(addedData.length, addedTitle, 'black');
                break;
            case 'temp' :
                addedData = mLatestData.temp;
                addedTitle = 'Temperature (°C)';
                addedRef = tempChartRef.current;      
                addedOption = options(addedData.length, addedTitle, 'red');
                break;
            default :
                addedData = [];
                addedTitle = 'Error';
                addedRef = ecChartRef.current;
                break;
        }
        let drawChart = () => {
            const data = new google.visualization.DataTable();
            data.addColumn('datetime', 'Time');
            data.addColumn('number', addedTitle);
            data.addRows(addedData);
            const chart1 = new google.visualization.LineChart(addedRef);
            chart1.draw(data, addedOption);
        }
        if (true) {
            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(drawChart);
        }
    }

    
    const onSearchClick = () => {
        const p = new Promise((resolve) => {
            let depthBuf = [];
            let ecBuf = [];
            let tempBuf = [];
            let filterData = queryData;
            if (filterData) {
                const { from, to } = dateRange;
                if (from) filterData = filterData.filter(data => data.date >= from)
                if (to) filterData = filterData.filter(data => data.date <= to)
                filterData.forEach(data => {
                    const { date, depth, ec, temp } = data || {};
                    depthBuf = [...depthBuf, [date, depth]]
                    ecBuf = [...ecBuf, [date, ec]]
                    tempBuf = [...tempBuf, [date, temp]]
                })
            }
            resolve({depth: depthBuf, ec: ecBuf, temp: tempBuf})
        })
        p.then(dataBuf => {
            setLatestData(dataBuf);
            //drawOneChart(dataBuf, key);
        })
    }

    const depthOption = {
        //width: 1000,//latestData.depth.length * 30 > 700 ? latestData.depth.length * 30 : 700,
        height: 350,
        chartArea : {
            width: '75%',
        },
        series: {
            0: {
                type: "line",
                targetAxisIndex: 0,
                color: "blue",
                pointSize: '5'
            },
        },
        vAxes: {
            0: {
                title: 'Groundwater level (m bgl)',
                titleTextStyle: {
                    italic: false,
                    bold: true,
                    color: 'blue'
                },
                direction: -1,
                viewWindow: {
                    min: 0,
                    max : 100,
                }
            },             
        },
        curveType: 'function',
        hAxis: {
            format: 'd/M/yy',
        },
    }
    

    return (
        <div >
            <DateForm dateRange={dateRange} setDateRange={setDateRange} onSearchClick={onSearchClick} />
            <hr />
            <Tabs id="tabsIotModal" className="mb-3" activeKey={key} onSelect={(k) => setKey(k)}>
                <Tab eventKey="depth" title="Groundwater level">
                    <div style={{overflowX: 'hidden', width: '100%', overflowY: 'hidden'}} >
                        <div ref={depthChartRef} />
                    </div>
                    <div className="text-end">* m bgl = meter below ground level</div>
                </Tab>
                <Tab eventKey="ec" title="Conductivity">
                    <div style={{overflowX: 'hidden', width: '100%', overflowY: 'hidden'}} >
                        <div ref={ecChartRef} />
                    </div>
                </Tab>
                <Tab eventKey="temp" title="Temperature">
                    <div style={{overflowX: 'hidden', width: '100%', overflowY: 'hidden'}} >
                        <div ref={tempChartRef} />
                    </div>
                </Tab>
                
            </Tabs>
        </div>
        
    )

}

const options = (l, n, c) => {
    return {
        //width: "100%",//l * 30 > 700 ? l * 30 : 700,
        height: 350,
        chartArea : {
            width: '75%',
        },
        series: {
            0: {
                type: "line",
                targetAxisIndex: 0,
                color: c,
                pointSize: '5'
            },
        },
        vAxes: {
            0: {
                title: n,
                titleTextStyle: {
                    italic: false,
                    bold: true,
                    color: c,
                },
            },                
        },
        curveType: 'function',
        hAxis: {
            format: 'd/M/yy',
        },
    }
}



export default IotScatterChart;

