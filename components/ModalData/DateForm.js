import { Col, Row } from "react-bootstrap";
import ReactDatePicker from "react-datepicker";


const DateForm = props => {

    const { dateRange, setDateRange, onSearchClick } = props;
    

    return (
        <div className="row" style={{alignItems: 'center'}}>
            <Row>
                <Col className="col-12 col-lg">
                    <Row>
                        <Col className="col-2 col-lg-auto align-self-center f-bold">From</Col>
                        <Col className="col col-lg dateForm">
                            <ReactDatePicker showYearDropdown yearDropdownItemNumber={15} scrollableYearDropdown
                                selected={dateRange.from} onChange={e => setDateRange({...dateRange, from : e || null})} dateFormat='dd/MM/yyyy'/>
                        </Col>
                    </Row>
                </Col>

                <Col className="col-12 col-lg">
                    <Row>
                        <Col className="col-2 col-lg-auto align-self-center f-bold">To</Col>
                        <Col className="col col-lg dateForm">
                            <ReactDatePicker showYearDropdown yearDropdownItemNumber={15} scrollableYearDropdown
                                selected={dateRange.to} onChange={e => setDateRange({...dateRange, to : e || null})} dateFormat='dd/MM/yyyy'/>
                        </Col>
                    </Row>
                </Col>

                <Col className="col col-lg-auto col-xl search-modal-btn">
                    <button className="modal-default-btn" onClick={onSearchClick}>search</button>
                </Col>
            </Row>
            
            
            
        </div>
    )
}

export default DateForm;