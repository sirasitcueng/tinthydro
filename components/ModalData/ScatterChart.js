import { useEffect, useRef, useState } from "react";

const ScatterChart = (props) => {

    const [latestData, setLatestData] = useState([]);
    const dataChartRef = useRef(null);
    
    const { type, dataObj } = props;

    useEffect(() => {
        let dataBuf = [];
        if (type !== 'd-excess') {
            for (const monthYear in dataObj) {
                const [year, month] = monthYear.split('-').map(v => parseInt(v));
                for (const day in dataObj[monthYear]) {
                    if (dataObj[monthYear][day] !== null) {
                        //setLatestData([...latestData, [new Date(year, month - 1, day), dataObj[monthYear][day]  ]])
                        dataBuf = [...dataBuf, [new Date(year, month - 1, day), dataObj[monthYear][day]  ]]
                    }
                }
            }
        }
        else {
            const {h2, o18} = dataObj;
            for (const monthYear in o18) {
                const [year, month] = monthYear.split('-').map(v => parseInt(v));
                if (h2[monthYear]) for (const day in h2[monthYear]) {
                    const h2Val = h2[monthYear][day];
                    const o18Val = o18[monthYear][day]
                    if (h2Val && o18Val){
                        const dx = parseFloat(h2Val) - 8 * parseFloat(o18Val);
                        dataBuf = [...dataBuf, [new Date(year, month - 1, day), dx  ]]
                    }
                }
            }

        }
        setLatestData([['time', type.toUpperCase()], ...dataBuf])
    }, [dataObj])

    useEffect(() => {
        //console.log(latestData)
        const drawChart = () => {
            const dataInTable = google.visualization.arrayToDataTable(latestData);
            const options = {
                //title: `${type.toUpperCase()}-time`,
                //width: 700,
                //height: 400,
                /*curveType: 'function',
                vAxis: {
                    viewWindow: {
                        max: 20,
                        min: 0,
                    }
                }*/
            }
            const chart = new google.visualization.ScatterChart(dataChartRef.current);
            chart.draw(dataInTable, options);
        }
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);
    }, [latestData]);

    return (
        <div>
            <div ref={dataChartRef} />
        </div>
        
    )
}


export default ScatterChart;