import { useContext, useEffect, useState } from "react"
import { Button, Col, Row } from "react-bootstrap";
import { LangContext } from "../../context/LangContext";
import { StationContext } from "../../context/StationContext";
import { TXT } from '../../context/TextDisplay';

const ShowImage = props => {
    const [imgList, setImgList] = useState({});
    const { lang } = useContext(LangContext);
    const { serverName } = useContext(StationContext);

    useEffect(() => {
        queryImageData();
        return () => {
            setImgList({});
        }
    }, []);

    const queryImageData = () => {
        getImageData(({...props, serverName})).then(imageObj => {
            imageObj.forEach(obj => {
                const imageBuffer = Buffer.from(obj.img.data.data)
                const blob = new Blob([imageBuffer], { type: obj.img.contentType });
                const url = URL.createObjectURL(blob)
                setImgList(l => ({...l, [obj._id] : {
                    name : obj.imgName,
                    des : obj.imgDesc,
                    url,
                    id : obj._id,
                }}))
            })
        })
    }

    const deleteBtn = (id) => {
        const onDeleteClick = async () => {
            await fetch(`${serverName}api/images`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json'
                },
                body : JSON.stringify({
                    id,
                })
            })
            .then(() => {
                setImgList(list => delete list[id]);
                alert(`ลบรูปภาพ ${id} สำเร็จ`)
            })
            .catch(e => console.log(e))
        };
        return (
            <Button variant="danger" onClick={onDeleteClick}>{TXT.DELETE_IMAGE[lang]}</Button>
        )
    }

    const MappingImage = () => {
        if (imgList) return Object.values(imgList).map((i, index) => (
            <Col md='6' key={index}>
                <div className="text-center"><img src={i.url} alt={i.name} height="200px" /></div>
                <div style={{fontWeight: 'bold'}}>{i.name}</div>
                <div>{i.des}</div>
                <div style={{textAlign: 'right'}}>{deleteBtn(i.id)}</div>
            </Col>
        ))
    }


    return (
        <div>
            <Row>
                <MappingImage />
            </Row>
        </div>
    )
}

const getImageData = async params => {
    const { serverName, type, sampleSiteName, sampleCode } = params;
    const searchParams = new URLSearchParams({
        type,
        sampleSiteName,
        sampleCode,
    })
    const rawData = await fetch(`${serverName}api/images?${searchParams}`, {
        method: 'GET',
    });
    const jsonData = await rawData.json();
    if (jsonData) return jsonData;
}

export default ShowImage;