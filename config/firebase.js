import { initializeApp } from "firebase/app"
import { getAuth, signInWithEmailAndPassword, signOut } from 'firebase/auth';

const firebaseConfig = {
    apiKey: "AIzaSyALGcoahSNIq2xsMADRkPpGF73c3tgQXFs",
    authDomain: "tint-hydro.firebaseapp.com",
    projectId: "tint-hydro",
    storageBucket: "tint-hydro.appspot.com",
    messagingSenderId: "755626363229",
    appId: "1:755626363229:web:d0f3f09706558f0c1fc0b4",
    measurementId: "G-NZ80CTHFJY"
};

const firebaseApp = initializeApp(firebaseConfig);

const auth = getAuth(firebaseApp);

export { auth, signInWithEmailAndPassword, signOut };